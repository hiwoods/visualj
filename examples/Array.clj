; This file may be updated with more meaningful and exhausted examples (to cover more cases and avoid bugs).
(step (line 3) (stack (frame main)) (stdout))
; Grammar: (Object-type object-id content ...)
; String begins with a capital letter because it's a reference type. Same goes for Array.
; Every object has a unique id (long). The unique ids this example shows are randomly made up.
(step (line 4) (stack (frame main (bind s (String 233 "hello array")))) (stdout))
(step (line 5) (stack (frame main (bind s (String 233 "hello array")) (bind a null)) (stdout))
; Right now, we only support array of primitive types. Other examples: (Array 234 0.0 0.0 0.0) (Array 234 true false true)
; (Array 234 a b c) for non-printable characters, ASCII escape characters will be used: (Array 234 \0 \001 \x02).
(step (line 6) (stack (frame main (bind s (String 233 "hello array")) (bind a (Array 234 0 0 0)))) (stdout))
(step (line 7) (stack (frame main (bind s (String 233 "hello array")) (bind a (Array 234 1 0 0)))) (stdout))
(step (line 8) (stack (frame main (bind s (String 233 "hello array")) (bind a (Array 234 1 2 0)))) (stdout))
(step (line 9) (stack (frame main (bind s (String 233 "hello array")) (bind a (Array 234 1 2 3)))) (stdout))
(step (line 9) (stack (frame main (bind s (String 233 "hello array")) (bind a (Array 234 1 2 3)) (ret void))) (stdout))