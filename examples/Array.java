import java.util.*;

public class Array {
    public static void main(String[] args) {
        String[] t2 = new String[2];
        
        ArrayList<String[]> list2 = new ArrayList<String[]>();
        
        t2[0]="0";
        t2[1]="0";
        list2.add(t2);
        System.out.println(list2.size());
        t2[0]="0";
        t2[1]="1";
        list2.add(t2);
        t2[0]="1";
        t2[1]="0";
        list2.add(t2);
        t2[0]="1";
        t2[1]="1";
        list2.add(t2);
        System.out.println(list2.size());

        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.pop();
        stack.pop();

        ArrayDeque<Integer> ad = new ArrayDeque<Integer>();
        ad.addFirst(1);
        ad.addFirst(2);
        ad.addFirst(3);
        ad.pollFirst();
        ad.pollFirst();
    }
    
    private static void set(int[] a) {
        for (int i = 0; i < a.length; ++i) {
            a[i] = 10;
        }
    }
}