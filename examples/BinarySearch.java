import java.util.Arrays;

public class BinarySearch {
    public static void main(String[] args) {
        int[] a = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
        int key = 7;
        String s = "Index of ";
        s += key;
        s += " is: ";
        s += binarySearch(a, key, 0, a.length);
        System.out.println(s);
    }

    /*
        corrupted version
     */
    private static int binarySearch(int[] a, int key, int lo, int hi) {
        if (lo >= hi) {
            return -1;
        } else {
            int mid = lo + (hi - lo) / 2;
            if (a[mid] == key) {
                return mid;
            } else if (a[mid] < key) {
                a = Arrays.copyOfRange(a, mid + 1, hi);
                lo = 0;
                hi = a.length;
                return binarySearch(a, key, lo, hi);
            } else {
                a = Arrays.copyOfRange(a, lo, mid);
                lo = 0;
                hi = a.length;
                return binarySearch(a, key, lo, hi);
            }
        }
    }

    /*
        correct version
     */
//    private static int binarySearch(int[] a, int key, int lo, int hi) {
//        if (lo >= hi) {
//            return -1;
//        } else {
//            int mid = lo + (hi - lo) / 2;
//            if (a[mid] == key) {
//                return mid;
//            } else if (a[mid] < key) {
////                a = Arrays.copyOfRange(a, mid + 1, hi);
//                lo = mid + 1; // fixed
////                hi = a.length;
//                return binarySearch(a, key, lo, hi);
//            } else {
////                a = Arrays.copyOfRange(a, lo, mid);
////                lo = 0;
//                hi = mid; // fixed
//                return binarySearch(a, key, lo, hi);
//            }
//        }
//    }

    /*
        old and corrupted version
     */
//    private static int binarySearch(int[] a, int key) {
//        if (a.length == 0) {
//            return -1;
//        } else {
//            int lo = 0;
//            int hi = a.length;
//            int mid = lo + (hi - lo) / 2;
//            if (a[mid] == key) {
//                return mid;
//            } else if (a[mid] < key) {
//                int[] newA = Arrays.copyOfRange(a, mid + 1, hi);
//                return binarySearch(newA, key);
//            } else {
//                int[] newA = Arrays.copyOfRange(a, lo, mid);
//                return binarySearch(newA, key);
//            }
//        }
//    }
}
