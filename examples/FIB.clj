; Note the indentation and newlines can be ignored when parsing. 
; it doesn't matter if a whole snapshot is written in one line, for example:
; (step (line 3) (stack (frame main)))
; To parse, we need to know
; how to match parenthesis, and tokens surrounded by parenthesis are delimited by spaces
; The parser should be easy to extend to add new grammar rules. 
; A new grammar is always like this: (grammar-name argument1 argument2 ....)
; This is probably the simplest grammar to parse when it comes to make compilers.

; syntax highlighting is available for this type of files on bitbucket

; The snapshots below match exactly what happens if you visualize "FIB.java" using JavaTutor
; So using JavaTutor can help understand the snapshots.

; Step 1 of 9; "line" indicates next line to execute (where the arrow points)
; next line to execute is an assignment operation
(step 
    (line 3)
    (stack 
        (frame main)))

; Step 2 of 9; Since fib needs to be called before assignment, nothing changes.
; next line to execute is call fib.
(step 
    (line 3)
    (stack 
        (frame main)))

; Step 3 of 9; fib is called, so a new frame with its parameter initialized is created.
; next line to execute is a comparison
(step 
    (line 8)
    (stack 
        (frame main)
        (frame fib
            (bind n 1))))

; Step 4 of 9; comparison is done, nothing changes.
; next line to execute is jump according to result of the comparison
(step 
    (line 8)
    (stack 
        (frame main)
        (frame fib
            (bind n 1))))

; Step 5 of 9; jump to a return statement
; next line to execute is return
(step 
    (line 9)
    (stack 
        (frame main)
        (frame fib
            (bind n 1))))

; Step 6 of 9; return value stored, end of the function call
; next line to execute is destroy the fib frame
(step 
    (line 10)
    (stack 
        (frame main)
        (frame fib
            (bind n 1)
            (ret 1))))

; Step 7 of 9; fib frame is destroyed
; next line to execute is an assignment operation
(step 
    (line 3)
    (stack 
        (frame main)))

; Step 8 of 9; assignment done, a string is defined. Since a string variable is a reference
; to a string object on heap and the prototype only supports primitive types, the content of
; the string will not be shown. "(ref)" indicates a reference.
; next line to execute is print
(step 
    (line 4)
    (stack 
        (frame main
            (bind s (ref)))))

; Step 9 of 9; "(stdout 1\n)" means System.out.print("1\n");
; next line to execute is return from main
(step 
    (line 5)
    (stack 
        (frame main
            (bind s (ref))))
    (stdout 1\n))

; Program terminated; no next line to execute, the arrow points to the next line
; to execute disappears
(step 
    (stack 
        (frame main
            (bind s (ref))
            (ret void)))
    (stdout 1\n))