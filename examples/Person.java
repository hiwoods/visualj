import java.util.ArrayList;

public class Person {
    private String name;
    private int age;
    private Person child;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setChild(Person child) {
        this.child = child;
    }

    public Person getChild() {
        return child;
    }

    public static void main(String[] args) {
        Person bob = new Person("Bob", 30);
        Person peter = new Person("Peter", 3);
        bob.setChild(peter);
        System.out.println(bob.getChild().name);
    }
}
