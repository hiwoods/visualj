public class FIB {
	final int max = 1000;
	static int[] mem = new int[1000];
	public static void main(String[] args) {
		String s = Integer.toString(fib(10));
//        String s = Integer.toString(fib_dp(10));
//        String s = Integer.toString(fib_dp1(10));
        System.out.println(s);
	}

	static int fib(int n) {
		if (n <= 0) {
			return n;
		} else {
			return fib(n-1) + fib(n-2);
		}
	}

	static int fib_dp(int n) {
		if (mem[n] != 0) {
		    return mem[n];
        } else {
            if (n <= 1) {
                return mem[n] = n;
            } else {
                return mem[n] = fib_dp(n-1) + fib_dp(n-2);
            }
        }
	}

    static int fib_dp1(int n) {
        if (mem[n] != 0) {
            return mem[n];
        } else {
            if (n <= 1) {
                return mem[n] = n;
            } else {
                return mem[n] = fib_dp1(n-2) + fib_dp1(n-1);
            }
        }
    }
}