package parser;

import utils.ParserFailedException;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse execution information to a StepManager
 * Created by Lam Cao on 4/17/2017.
 *
 * @see StepManager
 */
public class LegacyStepParser implements StepParser {

    /**
     * Pattern for frame
     */
    private final Pattern FRAME_PATTERN;
    /**
     * Pattern for line number + stack frames
     */
    private final Pattern LINE_AND_STACK_PATTERN;
    /**
     * Pattern for step
     */
    private final Pattern STEP_PATTERN;
    /**
     * Pattern for variable bindings
     */
    private final Pattern BINDING_PATTERN;
    /**
     * Pattern for return statement.
     */
    private final Pattern RETURN_PATTERN;
    /**
     * Pattern for heap objects.
     */
    private final Pattern VOBJECT_PATTERN;
    /**
     * Pattern for objects of an arbitrary class
     */
    private final Pattern ARBITRARY_CLASS_INSTANCE_PATTERN;
    /**
     * cache bufferReader
     */
    private BufferedReader tracerBufferedReader;
    /**
     * cache outputFile
     */
    private File tracerOutputFile;

    {
        FRAME_PATTERN = Pattern.compile("frame\\s(?:(?<NAME>[<>\\w_$]+)(?<CONT>.*))\\)$");
        LINE_AND_STACK_PATTERN = Pattern.compile("(?:\\(line\\s(?<LINE>\\d+)\\))\\s(?:\\(stack\\s(?<FRAME>.*)\\)\\s(?:\\(interface\\s?(?<INTERFACE>.*)\\))\\s(?:\\(exception\\s?(?<EXCEPTION>.*)\\))\\s(?:\\(stdout\\s?(?<STDOUT>.*)\\)))");
        STEP_PATTERN = Pattern.compile("^(?:\\(step\\s(?<ID>\\d+)\\s(?<CONT>.*))\\)$");
        BINDING_PATTERN = Pattern.compile("bind\\s(?<NAME>[\\w$]+):(?<TYPE>[\\w$:\\[\\]]+)\\s(?:(?:(?<REFERENCE>\\(.*)\\))|(?<PRIMITIVE>['\\w$-]+))");
        RETURN_PATTERN = Pattern.compile("ret\\s(?:(?<REFERENCE>\\(.*)\\)|(?<PRIMITIVE>[\\w$-]+))");
        VOBJECT_PATTERN = Pattern.compile("\\((?<TYPE>[\\w$]+)\\s(?<ID>\\d+)\\s?(?<VALUE>.*)\\)");
        ARBITRARY_CLASS_INSTANCE_PATTERN = Pattern.compile("\\((?<NAME>[\\w$\\[\\]]+)\\s(?<ID>\\d+)\\s?(?<FIELD>\\([\\w$:\\[\\]]+=(?:.*)\\))*\\)");
    }

    /**
     * Parse step information from a file
     *
     * @param file    information of program execution
     * @param atID    start at
     * @param newFile re-new cache
     * @return StepManager @see {@link StepManager}
     */
    @Override
    public List<Instruction> parseStep(File file, int atID, boolean newFile) throws ParserFailedException {
        List<Instruction> instrList = new ArrayList<>(50);
        try {
            if (newFile || tracerOutputFile == null || !tracerOutputFile.equals(file)) {
                tracerOutputFile = file;
                tracerBufferedReader = new BufferedReader(new FileReader(tracerOutputFile));
            }
            String line;
            Matcher matcher;
            boolean reached = atID == 0;
            while ((line = tracerBufferedReader.readLine()) != null) {
                if (!reached && line.startsWith("(step " + atID + " ")) {
                    reached = true;
                }
                if (reached && (matcher = STEP_PATTERN.matcher(line)).matches()) {
                    int id = Integer.parseInt(matcher.group("ID"));
                    Instruction instr = processStep(id, matcher.group("CONT"));
                    if (instr != null)
                        instrList.add(instr);

                } else {
                    throw new ParserFailedException("Error: can't parse " + line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (instrList.isEmpty())
            throw new ParserFailedException("Instruction set is empty");

        return instrList;
    }

    /**
     * Parse a step into an Instruction
     *
     * @param step information about step
     * @return an executable instruction
     */
    private Instruction processStep(int id, String step) throws ParserFailedException {
        Matcher m = LINE_AND_STACK_PATTERN.matcher(step);
        if (m.matches()) {
            int lineNumber = Integer.parseInt(m.group("LINE"));
            String frameInfo = m.group("FRAME");
            String interfaceInfo = m.group("INTERFACE");
            String exception = m.group("EXCEPTION");
            String outPut = m.group("STDOUT");
            return parseInstruction(id, lineNumber, outPut, frameInfo, interfaceInfo, exception);
        } else throw new ParserFailedException("Line and stack pattern mismatched.");
    }

    /**
     * convert step information to Instruction object
     *
     * @param lineNumber    line number
     * @param output        std output
     * @param frameInfo     raw step information
     * @param interfaceInfo interface info
     * @return Instruction object
     */
    private Instruction parseInstruction(int id, int lineNumber, String output, String frameInfo, String interfaceInfo, String exception) throws ParserFailedException {
        //Split frames to process individually.
        String[] frames = frameInfo.split("(?=\\s\\(frame)");
        ArrayList<VStack> listOfVStack = new ArrayList<>(50);
        LinkedHashMap<Integer, VObject> vObjectList = new LinkedHashMap<>();

        for (String f : frames) {
            Matcher m = FRAME_PATTERN.matcher(f);
            while (m.find()) {
                String stackName = m.group("NAME");
                String stackContent = m.group("CONT");

                listOfVStack.add(processStackContent(stackName, stackContent, vObjectList));
            }
        }
        return new Instruction(id, lineNumber, output, listOfVStack, vObjectList, parseInterface(interfaceInfo), parseException(exception));
    }

    /**
     * Parse exception info
     * @param exception exception info
     * @return VException object
     */
    private VException parseException(String exception) {
        Pattern p = Pattern.compile("([\\w._$]+)\\s(.*)");
        Matcher m = p.matcher(exception);
        if (m.matches())
            return new VException(m.group(1), m.group(2));
        else
            return null;
    }

    /**
     * process stack frame content (variables, values, etc.)
     *
     * @param stackContent contents of stack
     * @param vObjectList  a list to add vObjects if needed
     * @return Map of variables and values
     */
    private VStack processStackContent(String stackName, String stackContent, HashMap<Integer, VObject> vObjectList) throws ParserFailedException {

        if (stackContent == null || stackContent.equals(""))
            return new VStack(stackName, null, null);

        LinkedHashMap<String, String> varMap = new LinkedHashMap<>();
        LinkedHashMap<String, String> typeMap = new LinkedHashMap<>();
        //split the bindings
        String[] bindings = stackContent.split("(?=\\s\\((bind|ret))");

        for (String bind : bindings) {
            //binding statements
            if (bind.charAt(2) == 'b') {
                processBindings(bind, varMap, typeMap, vObjectList);
            } else if (bind.charAt(2) == 'r') {  //return statements
                processReturnStatement(bind, varMap, vObjectList);
            } else {
                throw new ParserFailedException("Binding statement has wrong format: " + bind);
            }
        }
        return new VStack(stackName, varMap, typeMap);
    }

    /**
     * Parse interface info and put into a hash map
     *
     * @param interfaces text
     * @return map of class and associated interfaces
     */
    private HashMap<String, LinkedList<String>> parseInterface(String interfaces) throws ParserFailedException {
        HashMap<String, LinkedList<String>> interfaceMap = new HashMap<>();

        if (interfaces != null && !interfaces.equals("")) {
            Pattern p = Pattern.compile("\\([\\s\\w]+\\)");
            Matcher m = p.matcher(interfaces);

            while (m.find()) {
                String[] content = m.group(0).split(" ");
                if (content.length >= 1) {
                    LinkedList<String> list = new LinkedList<>();
                    list.addAll(Arrays.asList(content).subList(1, content.length));
                    interfaceMap.put(content[0], list);
                } else {
                    throw new ParserFailedException("Error parsing interfaces " + m.group(0));
                }
            }
        }
        return interfaceMap;
    }

    /**
     * Interpret binding statements
     *
     * @param binding     binding information
     * @param varMap      variable map
     * @param vObjectList heap object lists
     */
    private void processBindings(String binding,
                                 LinkedHashMap<String, String> varMap,
                                 LinkedHashMap<String, String> typeMap,
                                 HashMap<Integer, VObject> vObjectList) throws ParserFailedException {
        Matcher m = BINDING_PATTERN.matcher(binding);
        while (m.find()) {
            String varName = m.group("NAME");
            String type = m.group("TYPE");
            String objectVal = m.group("REFERENCE");
            String primitiveVal = m.group("PRIMITIVE");

            if (primitiveVal != null) {
                varMap.put(varName, primitiveVal);
                typeMap.put(varName, type);
            } else if (objectVal != null) {
                String id = parseObjectInfo(objectVal, vObjectList);
                varMap.put(varName, id);
                typeMap.put(varName, type);
            } else {
                throw new ParserFailedException("Couldn't parse value of binding: " + binding);
            }
        }
    }

    /**
     * parse to VObject
     *
     * @param obj         raw information about that object
     * @param vObjectList heap object lists
     * @return raw converted data
     */
    private String parseObjectInfo(String obj, HashMap<Integer, VObject> vObjectList) throws ParserFailedException {
        Matcher m = ARBITRARY_CLASS_INSTANCE_PATTERN.matcher(obj);
        if (m.matches()) {
            String typeName = m.group("NAME");
            int objectID = Integer.parseInt(m.group("ID"));
            String objectVal = m.group("FIELD");

            if (vObjectList.containsKey(objectID)) {
                return "(" + objectID + ")";
            }

            LinkedHashMap<String, String> fields = new LinkedHashMap<>();
            LinkedHashMap<String, String> fieldTypes = new LinkedHashMap<>();

            if(objectVal != null) {
                String[] splitObject = splitFields(objectVal);
                if (splitObject.length > 0) {
                    for (String vObj : splitObject) {
                        Pattern p1 = Pattern.compile("\\((?<NAME>[\\w$\\[\\]]+):(?<TYPE>[\\w$\\[\\]]+)=(?<VALUE>.*)\\)");
                        Matcher m1 = p1.matcher(vObj);
                        if (!m1.matches()) {
                            System.err.println("Not a VObject. Returns with obj unmodified: " + vObj);
                            return vObj;
                        }
                        String fieldName = m1.group("NAME");
                        String fieldType = m1.group("TYPE");
                        String fieldValue = m1.group("VALUE");

                        fields.put(fieldName, parseObjectInfo(fieldValue, vObjectList));
                        fieldTypes.put(fieldName, fieldType);
                    }
                } else {
                    throw new ParserFailedException("Failed to split fields " + objectVal);
                }
            }

            vObjectList.put(objectID,
                    new VObject(objectID,
                            objectVal,
                            typeName, fields, fieldTypes));

            return "(" + objectID + ")";
        } else {
            m = VOBJECT_PATTERN.matcher(obj);

            if (!m.find())
                return obj;

            String typeName = m.group("TYPE");
            int objectID = Integer.parseInt(m.group("ID"));
            String objectVal = m.group("VALUE");

            if (vObjectList.containsKey(objectID)) {
                return "(" + objectID + ")";
            }

            StringBuilder objectReturningContent = new StringBuilder();
            String[] splitObject = splitObjectContent(objectVal);

            if (splitObject.length > 0) {
                for (String vObj : splitObject) {
                    objectReturningContent.append(parseObjectInfo(vObj, vObjectList)).append(" ");
                }
            } else {
                objectReturningContent.append(objectVal).append(" ");
            }
            objectReturningContent.deleteCharAt(objectReturningContent.length() - 1);

            vObjectList.put(objectID,
                    new VObject(objectID,
                            objectReturningContent.toString(),
                            VObjectType.parseVObjectType(typeName)));

            return "(" + objectID + ")";
        }
    }

    /**
     * Interpret return statements
     *
     * @param statement   the statement in String
     * @param varMap      variable map
     * @param vObjectList heap object lists
     */
    private void processReturnStatement(String statement, LinkedHashMap<String, String> varMap, HashMap<Integer, VObject> vObjectList) throws ParserFailedException {
        Matcher m = RETURN_PATTERN.matcher(statement);

        if (!m.find())
            throw new IllegalArgumentException("Cannot read statement: " + statement);

        String objectVal = m.group("REFERENCE");
        String primitiveVal = m.group("PRIMITIVE");

        if (primitiveVal != null)
            varMap.put("Return", primitiveVal);
        if (objectVal != null) {
            String id = parseObjectInfo(objectVal, vObjectList);
            varMap.put("Return", id);
        }
    }

    private String[] splitFields(String content) {
        ArrayList<String> list = splitFieldsHelper(content);
        return list.toArray(new String[list.size()]);
    }

    private ArrayList<String> splitFieldsHelper(String content) {
        if (content.equals(""))
            return new ArrayList<>();

        ArrayList<String> list = new ArrayList<>();

        char[] charArray = content.toCharArray();
        int left = 0;
        int endIdx = 0;
        boolean valStart = false;
        for (int i = 1; i < charArray.length; i++) {
            if (charArray[i] == '=')
                valStart = true;

            if (valStart && charArray[i] == '(')
                left++;
            if (valStart && charArray[i] == ')') {
                if (left == 0) {
                    endIdx = i + 1;
                    break;
                } else
                    left--;
            }
        }
        if (endIdx == content.length()) {
            list.add(content);
            return list;
        } else {
            list.add(content.substring(0, endIdx));
            list.addAll(splitFieldsHelper(content.substring(endIdx + 1)));
            return list;
        }

    }

    /**
     * split nested content of object by space
     * method assumes that the elements are either another ref objects or null
     */
    private String[] splitObjectContent(String content) {

        ArrayList<String> list = new ArrayList<>();
        char[] charArray = content.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == 'n'
                    && charArray[i + 1] == 'u'
                    && charArray[i + 2] == 'l'
                    && charArray[i + 3] == 'l') {
                list.add("null");
                i += 4;
            } else if (charArray[i] == '(') {
                int leftParentheses = 0;
                for (int k = i + 1; k < charArray.length; k++) {
                    if (charArray[k] == '(')
                        leftParentheses++;
                    else if (charArray[k] == ')') {
                        if (leftParentheses == 0) {
                            list.add(content.substring(i, k + 1));
                            i = k + 1;
                            break;
                        } else
                            leftParentheses--;
                    }
                }
            } else if (charArray[i] == '"') {
                for (int k = i + 1; k < charArray.length; k++) {
                    if (charArray[k] == '"' && charArray[k - 1] != '\\') {
                        list.add(content.substring(i, k + 1));
                        i = k;
                        break;
                    }
                }
            } else if (charArray[i] == '\'' && charArray[i + 2] == '\'') {
                list.add(content.substring(i, i + 3));
                i = i + 3;
            } else if (charArray[i] != ' ') {
                for (int k = i + 1; k <= charArray.length; k++) {
                    if (k == charArray.length || charArray[k] == ' ') {
                        list.add(content.substring(i, k));
                        i = k;
                        break;
                    }
                }
            }
        }
        return list.toArray(new String[list.size()]);
    }

    public static void main(String[] args) {
        StepParser parser = StepParser.createStepParser();
        String filename = "./src/parser/test.txt";
        List<Instruction> l = null;
        try {
            l = parser.parseStep(new File(filename), 0, true);
        } catch (ParserFailedException e) {
            e.printStackTrace();
        }
        assert l != null;
        for (Instruction instr : l)
            System.out.println(instr);
////        List<Instruction> l2 = parser.parseStep(new File(filename), 50, false);
////        System.out.println(l2);

        //System.out.println(((LegacyStepParser) parser).splitFields("(size=0) (elementData=(Array 104))"));
        //System.out.println(((LegacyStepParser) parser).splitFields("(size=1) (size1=2) (elementData=(Array 108 (Array 91 (String 106 \"0\") (String 106 \"0\")) null null null null null null null null null))"));
    }
}
