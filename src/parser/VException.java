package parser;

import java.io.Serializable;

/**
 * Object that represents a runtime error
 *
 * Created by Lam Cao
 */
public class VException implements Serializable{
    private static final long serialVersionUID = 1L;
    private String type;
    private String message;

    public VException(String type, String message) {
        this.type = type;
        this.message = message;
    }

    @Override
    public String toString() {
        return type + ": " + message;
    }
}