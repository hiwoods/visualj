package parser;

/**
 * Classify VObjects to fit their characteristics
 */
public enum VObjectType {
    ARRAY,
    STRING,
    GENERIC;

    /**
     * parse VObjectType
     * @param type type in string
     * @return VObjectType input represents.
     */
    public static VObjectType parseVObjectType(String type) {
        switch(type) {
            case "Array":
                return ARRAY;
            case "String":
                return STRING;
            default:
                return GENERIC;
        }
    }
}