package parser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Universal representation for all objects to be drawn in visualj
 */
public class VObject implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * ID of the object
     */
    private int ID;
    /**
     * raw data of object
     */
    private String rawData;
    /**
     * VObject Type
     */
    private VObjectType vObjType;
    /**
     * Custom type name. Usually a reference class
     */
    private String typeName;
    /**
     * field members. If there are any
     */
    private LinkedHashMap<String, String> fields;
    /**
     * Map of fields and their type
     */
    private LinkedHashMap<String, String> fieldTypes;

    VObject(int ID, String rawData, String typeName, LinkedHashMap<String, String> fields, LinkedHashMap<String, String> fieldTypes) {
        this.setID(ID);
        this.setRawData(rawData);
        this.setVObjType(VObjectType.GENERIC);
        this.setTypeName(typeName);
        this.setFields(fields);
        this.setFieldTypes(fieldTypes);
    }

    VObject(int ID, String rawData, VObjectType vObjType) {
        this.setID(ID);
        this.setRawData(rawData);
        this.setVObjType(vObjType);
    }

    public String getTypeName() {
        return typeName;
    }

    private void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public LinkedHashMap<String, String> getFields() {
        return fields;
    }

    private void setFields(LinkedHashMap<String, String> fields) {
        this.fields = fields;
    }

    /**
     * get object ID
     *
     * @return ID
     */
    public int getID() {
        return ID;
    }

    /**
     * set new ID
     *
     * @param ID new ID
     */
    private void setID(int ID) {
        this.ID = ID;
    }

    /**
     * get raw data of object.
     * <p>
     * Depends on type of data, it represents differently.
     * <p>
     * If it is an Array of primitive values. Raw data should look the same as in Array.clj (elements are separated by space)
     * If it is an Array of objects. Raw data should looks like (1)(7)(9) where 1, 7, 9 are IDs of other VObject in heaps.
     * If it is a String, raw data should look like "this is a string", with quotes.
     * If it is generic, it's something else not Array or String in heap.
     *
     * @return raw data
     */
    public String getRawData() {
        return rawData;
    }

    /**
     * set new rawData
     *
     * @param rawData new raw Data
     */
    private void setRawData(String rawData) {
        this.rawData = rawData;
    }

    /**
     * get object type
     *
     * @return Array if it's an Array, String if it is a String, Generic otherwise
     */
    public VObjectType getvObjType() {
        return vObjType;
    }

    /**
     * Change object type
     *
     * @param vObjType new object type
     */
    private void setVObjType(VObjectType vObjType) {
        if (vObjType != VObjectType.GENERIC)
            typeName = vObjType.name();
        this.vObjType = vObjType;
    }

    private List<String> toList(String content) {
        List<String> list = new ArrayList<>();
        switch (vObjType) {
            case ARRAY:
                String[] splitData = getRawData().split(" ");
                Collections.addAll(list, splitData);
                return list;
            case STRING:
                for (char c : content.toCharArray()) {
                    list.add(String.valueOf(c));
                }
                return list;
            case GENERIC:
                for (String fieldName : fields.keySet()) {
                    String value = fields.get(fieldName);
                    String type = fieldTypes.get(fieldName);
                    String toString;
                    if (value.startsWith("(") && value.endsWith(")"))
                        toString = String.format("%s={%s@%s}", fieldName, type, value.substring(1, value.length() - 1));
                    else
                        toString = String.format("%s={%s}:%s", fieldName, type, value);
                    list.add(toString);
                }
                return list;
            default:
                return list;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VObject)) return false;

        VObject vObject = (VObject) o;

        if (getID() != vObject.getID()) return false;
        if (!getRawData().equals(vObject.getRawData())) return false;
        return vObjType == vObject.vObjType;
    }

    @Override
    public int hashCode() {
        int result = getID();
        result = 31 * result + getRawData().hashCode();
        result = 31 * result + vObjType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "VObject{" +
                "ID=" + ID +
                ", rawData='" + rawData + '\'' +
                ", vObjType=" + vObjType +
                ", typeName='" + typeName + '\'' +
                ", fields=" + fields +
                '}';
    }

    /**
     * split raw data to manageable list
     *
     * @return list
     */
    public List<String> toList() {
        return toList(getRawData());
    }

    /**
     * types of field members. If there are any
     */
    public LinkedHashMap<String, String> getFieldTypes() {
        return fieldTypes;
    }

    private void setFieldTypes(LinkedHashMap<String, String> fieldTypes) {
        this.fieldTypes = fieldTypes;
    }

    public static void main(String[] args) {
        String test = "1 23 45 6 7 8";
        String test2 = "\"this is a string\"";
        String test3 = "(123) (456) (789)";

        VObject v = new VObject(123, test, VObjectType.ARRAY);
        VObject v2 = new VObject(123, test2, VObjectType.STRING);
        VObject v3 = new VObject(123, test3, VObjectType.ARRAY);

        System.out.println(v.toList());
        System.out.println(v2.toList());
        System.out.println(v3.toList());
    }

}