package parser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Data representation of Stack frame
 * <p>
 * Created by Lam Cao on 9/28/2017.
 */
public class VStack implements Serializable {
    private static final long serialVersionUID = 1L;
    private String stackName;
    private HashMap<String, String> varMap;
    private HashMap<String, String> typeMap;
    private ArrayList<VVariable> variables;

    public VStack(String stackName, HashMap<String, String> varMap, LinkedHashMap<String, String> typeMap) {
        this.stackName = stackName;
        this.varMap = varMap;
        this.typeMap = typeMap;
        this.variables = new ArrayList<>();

        if (varMap == null && typeMap == null)
            return;

        assert varMap != null;
        assert typeMap != null;
        for (Map.Entry<String, String> var : varMap.entrySet()) {
            variables.add(new VVariable(var.getKey(), typeMap.get(var.getKey()), var.getValue()));
        }
    }

    public String getStackName() {
        return stackName;
    }

    public ArrayList<VVariable> getVariables() {
        return variables;
    }

    public HashMap<String, String> getVarMap() {
        return varMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VStack)) return false;

        VStack vStack = (VStack) o;

        if (!getStackName().equals(vStack.getStackName())) return false;
        return getVarMap() != null ? getVarMap().equals(vStack.getVarMap()) : vStack.getVarMap() == null;
    }

    @Override
    public int hashCode() {
        int result = getStackName().hashCode();
        result = 31 * result + (getVarMap() != null ? getVarMap().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "VStack{" +
                "stackName='" + stackName + '\'' +
                ", varMap=" + varMap +
                '}';
    }

    public HashMap<String, String> getTypeMap() {
        return typeMap;
    }

    private void setTypeMap(HashMap<String, String> typeMap) {
        this.typeMap = typeMap;
    }
}
