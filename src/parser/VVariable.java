package parser;

import java.io.Serializable;

/**
 * A variable that exists at runtime
 * <p>
 * Created by Lam Cao on 11/28/2017.
 */
public class VVariable implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String type;
    private String value;

    public VVariable(String name, String type, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VVariable)) return false;

        VVariable vVariable = (VVariable) o;

        if (getName() != null ? !getName().equals(vVariable.getName()) : vVariable.getName() != null) return false;
        if (getType() != null ? !getType().equals(vVariable.getType()) : vVariable.getType() != null) return false;
        return getValue() != null ? getValue().equals(vVariable.getValue()) : vVariable.getValue() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        if (name.equals("Return"))
            return name + "=" + value;
        if (value.startsWith("(") && value.endsWith(")"))
            return String.format("%s={%s@%s}", name, type, value.substring(1, value.length() - 1));
        return String.format("%s={%s}:%s", name, type, value);
    }
}
