package parser;

import java.io.*;
import java.util.*;

/**
 * This class represents a step of a java program.
 * <p>
 * Created by hiwoods on 3/28/2017.
 */
public class Instruction implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String CACHE_DIRECTORY = "cache/";
    /**
     * ID
     */
    private int id;
    /**
     * Line number for the particular step
     */
    private int lineNumber;
    /**
     * Output to console
     */
    private String stdOutPut;

    /**
     * List of VStack frames
     */
    private List<VStack> listOfVStack;
    /**
     * List of Objects in Heap
     */
    private LinkedHashMap<Integer, VObject> listOfVObject;
    /**
     * Map of interfaces used in program
     */
    private HashMap<String, LinkedList<String>> interfaces;
    /**
     * Exception if there is any
     */
    private VException exception;

    public Instruction(int id, int lineNumber, String outPut, List<VStack> listOfVStack, LinkedHashMap<Integer, VObject> listOfVObject, HashMap<String, LinkedList<String>> interfaces, VException exception) {
        this.setId(id);
        this.setLineNumber(lineNumber);
        this.setStdOutPut(outPut);
        this.setListOfVStack(listOfVStack);
        this.setListOfVObject(listOfVObject);
        this.setInterfaces(interfaces);
        this.exception = exception;
    }

    public VException getException() {
        return exception;
    }

    /**
     * get ID
     *
     * @return ID
     */
    public int getId() {
        return id;
    }

    /**
     * set new ID
     *
     * @param id newID
     */
    private void setId(int id) {
        this.id = id;
    }

    /**
     * Get output
     *
     * @return std output
     */
    public String getStdOutPut() {
        return stdOutPut;
    }

    /**
     * set new std output
     *
     * @param stdOutPut new std output
     */
    private void setStdOutPut(String stdOutPut) {
        if (stdOutPut == null)
            this.stdOutPut = "";
        else
            this.stdOutPut = stdOutPut;
    }

    /**
     * Set new interface map
     *
     * @param interfaces new map
     */
    private void setInterfaces(HashMap<String, LinkedList<String>> interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * get interface map
     *
     * @return interface map
     */
    public HashMap<String, LinkedList<String>> getInterfaces() {
        return interfaces;
    }

    /**
     * get line number
     *
     * @return line number
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * change line number
     *
     * @param lineNumber new line number
     */
    private void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * set new list of stack frames
     *
     * @param listOfVStack stack frames
     */
    private void setListOfVStack(List<VStack> listOfVStack) {
        if (listOfVStack != null) {
            this.listOfVStack = listOfVStack;
        } else
            System.err.println("Attempted to assign null list of stack");
    }

    /**
     * Return list of VStack
     *
     * @return VStack list
     */
    public List<VStack> getListOfVStack() {
        return listOfVStack;
    }

    /**
     * get objects in heaps
     *
     * @return map of VObject
     */
    public Map<Integer, VObject> getMapOfVObject() {
        return listOfVObject;
    }

    /**
     * get objects in heaps
     *
     * @return list of VObject
     */
    public List<VObject> getListOfVObject() {
        return new ArrayList<>(listOfVObject.values());
    }

    /**
     * set new objects in heap
     *
     * @param listOfVObject new list of VObject
     */
    private void setListOfVObject(LinkedHashMap<Integer, VObject> listOfVObject) {
        this.listOfVObject = listOfVObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Instruction)) return false;

        Instruction that = (Instruction) o;

        if (id != that.id) return false;
        if (lineNumber != that.lineNumber) return false;
        if (stdOutPut != null ? !stdOutPut.equals(that.stdOutPut) : that.stdOutPut != null) return false;
        if (listOfVStack != null ? !listOfVStack.equals(that.listOfVStack) : that.listOfVStack != null) return false;
        return listOfVObject != null ? listOfVObject.equals(that.listOfVObject) : that.listOfVObject == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + lineNumber;
        result = 31 * result + (stdOutPut != null ? stdOutPut.hashCode() : 0);
        result = 31 * result + (listOfVStack != null ? listOfVStack.hashCode() : 0);
        result = 31 * result + (listOfVObject != null ? listOfVObject.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Instruction{" +
                "id=" + id +
                ", lineNumber=" + lineNumber + "\n" +
                "\t stdOutPut='" + stdOutPut + '\'' + "\n" +
                "\t listOfVStack=" + listOfVStack + "\n" +
                "\t listOfVObject=" + listOfVObject +
                '}';
    }

    /**
     * return the dimension of the array. 0 means the object is not an array
     *
     * @param id id of object
     * @return k-dimension of the array
     */
    public int isKdArray(String id) {
        if (id.matches("\\(\\d+\\)"))
            return isKdArray(Integer.parseInt(id.substring(1, id.length() - 1)));
        return 0;
    }

    /**
     * return the dimension of the array. 0 means the object is not an array
     *
     * @param id id of object
     * @return k-dimension of the array
     */
    public int isKdArray(int id) {
        if (!listOfVObject.containsKey(id))
            return 0;
        VObject obj = listOfVObject.get(id);

        if (obj.getvObjType() == VObjectType.ARRAY) {
            return 1 + isKdArray(obj.toList().get(0));
        } else {
            return 0;
        }
    }

    /*
    * **********************************SERIALIZATION PART***********************************************
    */

    /**
     * serialize a list of Instruction into bucket+id.txt
     *
     * @param list list of instr to serialize
     * @param id   bucket id
     */
    static void serialize(List<Instruction> list, int id) {
        System.out.println("Serializing bucket " + id);
        serialize(list, CACHE_DIRECTORY + "bucket" + id + ".txt");
    }

    /**
     * serialize a list of Instruction into file name
     *
     * @param list list of instr to serialize
     * @param name file name
     */
    private static void serialize(List<Instruction> list, String name) {
        try {
            File file = new File(name);
            if (file.exists())
                file.delete();
            file.createNewFile();
            ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(name, true));
            for (Instruction instr : list) {
                outStream.writeObject(instr);
            }
            outStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Read and deserialize bucket with id
     *
     * @param id id of a bucket
     * @return list of Instruction
     */
    static List<Instruction> deserialize(int id) {
        System.out.println("Deserializing bucket " + id);
        return deserialize(CACHE_DIRECTORY + "bucket" + id + ".txt");
    }

    /**
     * Read and deserialize bucket with id
     *
     * @param name file name
     * @return list of Instruction
     */
    @SuppressWarnings("unchecked")
    private static List<Instruction> deserialize(String name) {
        try {
            List<Instruction> obj = new ArrayList(100);
            File file = new File(name);
            ObjectInputStream objectinputstream = new ObjectInputStream(new FileInputStream(file));
            try {
                while (true) {
                    obj.add((Instruction) objectinputstream.readObject());
                }
            } catch (EOFException ignored) {
                //System.out.println("EOF ignored");
            }
            objectinputstream.close();
            return obj;
        } catch (Exception e) {
            System.err.println("Can't deserialize given name: " + name + ". Method is returning null. Error: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /*
    Unit test
     */
    public static void main(String[] args) {
        List<Instruction> list;
        try {
            list = StepParser.createStepParser().parseStep(new File("examples/Array.txt"), 0, true);
            Instruction.serialize(list, CACHE_DIRECTORY + "bucket0.txt");

            for (Instruction instr : list) {
                System.out.println(instr.toString());
            }
            List<Instruction> list2 = Instruction.deserialize(CACHE_DIRECTORY + "bucket0.txt");

            System.out.println("Are two lists the same? " + list.equals(list2));

            System.out.println("Array 91 is " + list.get(0).isKdArray("(91)") + "d");
            System.out.println("Array 91 is " + list.get(0).isKdArray(91) + "d");

            System.out.println("Array 91 is " + list.get(1).isKdArray("(91)") + "d");
            System.out.println("Array 91 is " + list.get(1).isKdArray(91) + "d");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
