package parser;

import tracer.Tracer;
import utils.InvalidCodeException;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Monitor steps during execution
 * Created by Hiwoods on 3/29/2017.
 */
public class StepManager extends Observable {
    /**
     * Default request size
     */
    private static final int REQUEST_SIZE = 100;
    /**
     * Default request size
     */
    private static final int MAX_BUCKET = 5;
    /**
     * List of instruction of a java program
     */
    private HashMap<Integer, List<Instruction>> instructions;
    private int minBucketLoaded = 0;
    private int maxBucketLoaded = 0;
    private int absoluteMaxBucketLoaded = 0;
    /**
     * Tracer
     */
    private Tracer tracer;
    /**
     * Tracer
     */
    private File tracerOutPutFile;
    private StepParser parser;

    public StepManager(File sourceFile) throws InvalidCodeException {
        try {
            tracer = new Tracer(sourceFile);
            int n = tracer.getSteps(REQUEST_SIZE);

            if (n < REQUEST_SIZE)
                tracer.close();

            String className = sourceFile.getName().substring(0, sourceFile.getName().length() - 5);
            tracerOutPutFile = new File(className + ".txt");
            parser = StepParser.createStepParser();
            instructions = new HashMap<>();
            loadInstruction(parser.parseStep(tracerOutPutFile, 0, true));
        } catch (Exception e) {
            String message = e.getMessage();

            if(message != null) {
                Pattern p = Pattern.compile("(\\d+)\\s(.*)");
                Matcher m = p.matcher(message);
                if (m.matches()) {
                    String line = m.group(1);
                    String error = m.group(2);
                    throw new InvalidCodeException("Line " + line + ": " + error);
                } else
                    throw new InvalidCodeException(message);
            } else {
                e.printStackTrace();
            }
        }
    }

    private void requestMoreSteps(int atID) {
        try {
            int n = tracer.getSteps(REQUEST_SIZE);
            if (n > 0) {
                List<Instruction> list = parser.parseStep(tracerOutPutFile, atID, false);
                loadInstruction(list);
            }
            if (n < REQUEST_SIZE)
                tracer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadInstruction(List<Instruction> list) {
        int bucket = list.get(0).getId() / REQUEST_SIZE;
        int lastInstrBucket = list.get(list.size() - 1).getId() / REQUEST_SIZE;
        if (bucket == lastInstrBucket) {
            instructions.put(bucket, list);
            setMinOrMaxBucketLoaded(bucket);

            if (instructions.size() > MAX_BUCKET) {
                if (bucket < (maxBucketLoaded + minBucketLoaded) / 2)
                    cutHead();
                else
                    cutTail();
            }
        } else {
            //list contains elements of different buckets
            throw new IllegalArgumentException("Trying to add instr into nonexist bucket " + bucket);
        }
    }

    private void cutHead() {
        removeBucket(maxBucketLoaded);
        checkMinAndMaxBucketLoaded();
    }

    private void cutTail() {
        removeBucket(minBucketLoaded);
        checkMinAndMaxBucketLoaded();
    }

    private void removeBucket(int id) {
        Instruction.serialize(instructions.get(id), id);
        instructions.remove(id);
    }

    private void checkMinAndMaxBucketLoaded() {
        minBucketLoaded = Integer.MAX_VALUE;
        maxBucketLoaded = Integer.MIN_VALUE;
        for (int i : instructions.keySet())
            setMinOrMaxBucketLoaded(i);
    }

    private void setMinOrMaxBucketLoaded(int bucket) {
        if (minBucketLoaded > bucket) {
            minBucketLoaded = bucket;
        }
        if (maxBucketLoaded < bucket) {
            maxBucketLoaded = bucket;
            if (maxBucketLoaded > absoluteMaxBucketLoaded) {
                absoluteMaxBucketLoaded = maxBucketLoaded;
            }
        }
    }

    /**
     * Get last Instruction ID
     */
    public int getLastInstructionID() {
        List<Instruction> list = instructions.get(maxBucketLoaded);
        return list.get(list.size() - 1).getId();
    }

    /**
     * Get first Instruction ID
     */
    public int getFirstInstructionID() {
        List<Instruction> list = instructions.get(minBucketLoaded);
        return list.get(0).getId();
    }

    public int getInstructionsCount() {
        int sum = 0;
        for (List<Instruction> list : instructions.values())
            sum += list.size();
        return sum;
    }

    /**
     * Get list of instruction
     * Deprecated because it no longer guarantees order of instructions
     *
     * @return lists of Instruction
     */
    @Deprecated
    public List<Instruction> getInstructions() {
        List<Instruction> list = new ArrayList<>();
        for (List<Instruction> l : instructions.values())
            list.addAll(l);
        return list;
    }

    /**
     * Jump to an indexed step
     *
     * @param index index of instruction in array
     * @return Instruction at that index
     */
    public Instruction jumpTo(int index) {
        if (index < 0 || index > getLastInstructionID()) {
            throw new ArrayIndexOutOfBoundsException("Step index " + index + " is out side of Instruction list size of " + getLastInstructionID());
        }
        int bucket = index / REQUEST_SIZE;

        //if bucket is not in memory, load from disk
        if (!instructions.containsKey(bucket) && bucket <= absoluteMaxBucketLoaded) {
            loadInstruction(Instruction.deserialize(bucket));
        }
        Instruction instr = instructions.get(bucket).get(index - REQUEST_SIZE * bucket);
        //index and instr's ID should match
        assert instr.getId() == index;
        if (index == getLastInstructionID()) {
            if (bucket == absoluteMaxBucketLoaded)
                requestMoreSteps(getLastInstructionID() + 1);
            else
                loadInstruction(Instruction.deserialize(bucket + 1));
        }
        //pass instruction to visualizerUI
        setChanged();
        notifyObservers(instr);
        return instr;
    }
}
