package parser;

import utils.ParserFailedException;

import java.io.File;
import java.util.List;

/**
 * Step parser interface
 *
 * Created by Lam Cao on 10/9/2017.
 */
public interface StepParser {

    static StepParser createStepParser() {
        return new LegacyStepParser();
    }

    List<Instruction> parseStep(File file, int atID, boolean newFile) throws ParserFailedException;
}
