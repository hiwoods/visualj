package utils;

import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * A straight line that have 2 circles at end points
 * <p>
 * Created by Lam Cao
 */
public class ArrowLine extends Parent {
    private static final double DEFAULT_RADIUS = 5;
    private Line line;
    private Circle startPoint, endPoint;

    public ArrowLine(Point2D start, Point2D end) {
        this(start.getX(), start.getY(), end.getX(), end.getY());
    }

    public ArrowLine(double x1, double y1, double x2, double y2) {
        line = new Line(x1, y1, x2, y2);
        line.setStrokeWidth(3f);
        line.setMouseTransparent(true);
        startPoint = new Circle(x1, y1, DEFAULT_RADIUS, Color.BLACK);
        endPoint = new Circle(x2, y2, DEFAULT_RADIUS, Color.RED);
        line.startXProperty().bind(startPoint.centerXProperty());
        line.startYProperty().bind(startPoint.centerYProperty());
        line.endXProperty().bind(endPoint.centerXProperty());
        line.endYProperty().bind(endPoint.centerYProperty());
        getChildren().addAll(line, startPoint, endPoint);
        setMouseTransparent(true);
    }

    public Circle getStartPoint() {
        return startPoint;
    }

    public Circle getEndPoint() {
        return endPoint;
    }
}