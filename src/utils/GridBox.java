package utils;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.Arrays;
import java.util.List;

/**
 * A custom grid view. It can show index row and column
 * Created by Lam Cao on 12/4/2017.
 */
public class GridBox extends VBox {

    private static final double DEFAULT_BORDER_WIDTH = 0.5;
    private static final Color DEFAULT_BORDER_COLOR = Color.BLACK;
    public static final Color DEFAULT_CELL_HIGHLIGHT_COLOR = Color.web("#f9f1a4");
    public static final double DEFAULT_CELL_WIDTH_PADDING = 5f;
    public static final double DEFAULT_CELL_HEIGHT_PADDING = 0f;
    public static final double DEFAULT_CELL_WIDTH = 48f;
    public static final double DEFAULT_CELL_HEIGHT = 32f;
    private static final double DEFAULT_INDEX_CELL_WIDTH = 16f;
    private static final double DEFAULT_INDEX_CELL_HEIGHT = 16f;

    private int numRow = 0;
    private int numCol = 0;
    private double maxCellWidth = DEFAULT_CELL_WIDTH;
    private double maxCellHeight = DEFAULT_CELL_HEIGHT;
    private boolean bottomIndexRowEnabled = false;
    private boolean leftIndexColEnabled = false;
    private double horizontalGap;
    private double verticalGap;

    public GridBox(double cellWidth, double cellHeight, double hGap, double vGap) {
        super();
        setFillWidth(false);

        setMaxCellWidth(cellWidth);
        setMaxCellHeight(cellHeight);
        horizontalGap = hGap;
        verticalGap = vGap;
    }

    public void removeRow(int index) {
        getChildren().remove(index);
        numRow--;
    }

    public void addRows(StackPane... nodes) {
        addRows(Arrays.asList(nodes));
    }

    public void addRows(List<StackPane> nodes) {
        HBox rowBox = new HBox();
        rowBox.setFillHeight(false);
        for (StackPane n : nodes) {
            rowBox.getChildren().add(n);
        }

        getChildren().add(rowBox);
        numRow++;
        if (numCol < nodes.size())
            numCol = nodes.size();
    }

    public void setLeftIndexColVisible(boolean b) {
        if (b == leftIndexColEnabled)
            return;

        for (int i = 0; i < numRow; i++) {
            HBox row = (HBox) getChildren().get(i);
            if (b) {
                StackPane stack = makeIndexCell(i + "",
                        DEFAULT_INDEX_CELL_WIDTH,
                        maxCellHeight);
                row.getChildren().add(0, stack);
            } else
                row.getChildren().remove(0);
        }

        if (bottomIndexRowEnabled) {
            StackPane stack = makeIndexCell("",
                    DEFAULT_INDEX_CELL_WIDTH,
                    maxCellHeight);
            ((HBox) getChildren().get(getChildren().size() - 1)).getChildren().add(0, stack);
        }
        leftIndexColEnabled = b;
    }

    public void setBottomIndexRowVisible(boolean b) {
        if (b == bottomIndexRowEnabled)
            return;
        if (b) {
            HBox rowBox = new HBox();
            rowBox.setFillHeight(false);
            if (leftIndexColEnabled) {
                //divided by 2 to get closer to cell
                StackPane stack = makeIndexCell("", DEFAULT_INDEX_CELL_WIDTH, DEFAULT_INDEX_CELL_HEIGHT);
                rowBox.getChildren().add(stack);
            }
            for (int i = 0; i < numCol; i++) {
                //divided by 2 to get closer to cell
                StackPane stack = makeIndexCell(i + "", maxCellWidth, DEFAULT_INDEX_CELL_HEIGHT);
                rowBox.getChildren().add(stack);
            }
            getChildren().add(getChildren().size(), rowBox);
        } else {
            getChildren().remove(getChildren().size() - 1);
        }
        bottomIndexRowEnabled = b;
    }

    private StackPane makeIndexCell(String content, double width, double height) {
        StackPane stack = new StackPane();
        Rectangle rect = makeRectangle(width, height, Color.TRANSPARENT, false);
        Text text = new Text(content);
        stack.getChildren().add(rect);
        stack.getChildren().add(text);
        return stack;
    }

    public Rectangle makeRectangle(double width, double height, Color fillColor, boolean border) {
        Rectangle rect = new Rectangle(width + verticalGap + DEFAULT_CELL_WIDTH_PADDING, height + horizontalGap);
        rect.setFill(fillColor);

        if (border) {
            rect.setStroke(DEFAULT_BORDER_COLOR);
            rect.setStrokeWidth(DEFAULT_BORDER_WIDTH);
        } else {
            rect.setStroke(Color.TRANSPARENT);
            rect.setStrokeWidth(DEFAULT_BORDER_WIDTH);
        }

        return rect;
    }

    public void refreshSize() {
        int startCol = leftIndexColEnabled ? 1 : 0;
        for (Node node : getChildren()) {
            HBox rowBox = (HBox) node;
            for (int i = startCol; i < rowBox.getChildren().size(); i++) {
                StackPane pane = (StackPane) rowBox.getChildren().get(i);
                Rectangle rect = (Rectangle) pane.getChildren().get(0);
                if (rect.getWidth() != maxCellWidth)
                    rect.setWidth(maxCellWidth);
                if (rect.getHeight() != maxCellHeight)
                    rect.setHeight(maxCellHeight);
            }
        }
        this.autosize();
    }

    public int getNumRow() {
        return numRow;
    }

    public int getNumCol() {
        return numCol;
    }

    public double getMaxCellWidth() {
        return maxCellWidth;
    }

    public double getMaxCellHeight() {
        return maxCellHeight;
    }

    public boolean isBottomIndexRowEnabled() {
        return bottomIndexRowEnabled;
    }

    public boolean isLeftIndexColEnabled() {
        return leftIndexColEnabled;
    }

    public void setMaxCellWidth(double maxCellWidth) {
        double widthWithPadding = maxCellWidth + DEFAULT_CELL_WIDTH_PADDING;
        if (widthWithPadding > this.maxCellWidth)
            this.maxCellWidth = widthWithPadding;
        else if (widthWithPadding >= DEFAULT_CELL_WIDTH) {
            this.maxCellWidth = widthWithPadding;
        }
    }

    public void setMaxCellHeight(double maxCellHeight) {
        double heightWithPadding = maxCellHeight + DEFAULT_CELL_HEIGHT_PADDING;
        if (heightWithPadding > this.maxCellHeight)
            this.maxCellHeight = heightWithPadding;
        else if (heightWithPadding >= DEFAULT_CELL_HEIGHT) {
            this.maxCellHeight = heightWithPadding;
        }
    }
}
