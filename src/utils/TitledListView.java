package utils;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * A list view with a title
 * <p>
 * Created by Lam Cao on 11/27/2017.
 */
public class TitledListView<T> extends VBox {

    @FXML
    private Label titleLabel;
    private ListView<T> itemListView;

    public TitledListView() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/utils/TitledListView.fxml")
        );

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        itemListView = new ListView<>();
        itemListView.setPrefSize(ListView.USE_COMPUTED_SIZE, ListView.USE_COMPUTED_SIZE);
        getChildren().add(itemListView);
    }

    public void setTitleLabel(String value) {
        titleLabel.setText(value);
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public ListView<T> getItemListView() {
        return itemListView;
    }
}