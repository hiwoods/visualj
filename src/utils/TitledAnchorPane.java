package utils;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.io.IOException;

/**
 * A list view with a title
 * <p>
 * Created by Lam Cao on 11/27/2017.
 */
public class TitledAnchorPane extends VBox {

    public final static double TITLE_OVERHEAD = 18f;
    @FXML
    private Label titleLabel;
    @FXML
    private AnchorPane containerPane;

    private String originalTitle;

    public TitledAnchorPane() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/utils/TitledAnchorPane.fxml")
        );

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setTitleLabel(String value) {
        originalTitle = value;
        refreshSize();
    }

    public String getTitleLabel() {
        return titleLabel.getText();
    }

    public AnchorPane getContainerPane() {
        return containerPane;
    }

    public boolean getTitleVisibility() {
        return titleLabel.isVisible();
    }

    public void setTitleVisible(boolean state) {
        if (state) {
            titleLabel.setMaxHeight(16);
            titleLabel.setPrefHeight(16);
        } else {
            titleLabel.setMinHeight(0);
            titleLabel.setMaxHeight(0);
        }
        titleLabel.setVisible(state);
    }

    public void showBorder(boolean b) {
        if (b)
            containerPane.setStyle("-fx-border-width: 1; -fx-border-color: black;");
        else
            containerPane.getStylesheets().clear();
    }

    public void refreshSize() {
        autosize();
        containerPane.autosize();
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        double titleTextWidth = Math.ceil(fontLoader.computeStringWidth(originalTitle, Font.getDefault()));
        double containerWidth = containerPane.getWidth();

        if (containerWidth < titleTextWidth) {

            double ratio = containerWidth / titleTextWidth;
            int subLength = (int) (originalTitle.length() * ratio);
            String newLabel = originalTitle.substring(0, subLength - 2) + "...";
            titleLabel.setText(newLabel);
        } else {
            titleLabel.setText(originalTitle);
        }

    }
}