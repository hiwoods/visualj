package utils;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * A test stage
 * Created by Lam Cao on 12/4/2017.
 */
public class TestStage extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
//        AnchorPane root = new AnchorPane();
//        GridBox pane = new GridBox(64, 32, 1, 1);
//
//        pane.addRows(getSomething());
//        pane.addRows(getSomething2());
//        //pane.removeIndicesBottom();
//        pane.addRows(getSomething());
//        pane.addRows(getSomething());
//
//        pane.addRows(getSomething());
//        pane.addRows(getSomething());
//
//        pane.addRows(getSomething());
//        pane.addRows(getSomething());
//
//        pane.setLeftIndexColVisible(true);
//        pane.setBottomIndexRowVisible(true);
//        //pane.setBottomIndexRowVisible(false);
//        pane.refreshSize();
//        root.getChildren().add(pane);
//        Scene scene = new Scene(root, 600, 400);
//        primaryStage.setScene(scene);
//        primaryStage.setTitle("Java Keywords Demo");
//        primaryStage.show();
        DraggableTab tab1 = new DraggableTab("Tab 1");
        //tab1.setClosable(false);
        //tab1.setDetachable(false);
        tab1.setContent(new Rectangle(500, 500, Color.BLACK));
        DraggableTab tab2 = new DraggableTab("Tab 2");
        tab2.setClosable(false);
        tab2.setContent(new Rectangle(500, 500, Color.RED));
        DraggableTab tab3 = new DraggableTab("Tab 3");
        tab3.setClosable(false);
        tab3.setContent(new Rectangle(500, 500, Color.BLUE));
        DraggableTab tab4 = new DraggableTab("Tab 4");
        tab4.setClosable(false);
        tab4.setContent(new Rectangle(500, 500, Color.ORANGE));
        TabPane tabs = new TabPane();
        tabs.getTabs().add(tab1);
        tabs.getTabs().add(tab2);
        tabs.getTabs().add(tab3);
        tabs.getTabs().add(tab4);

        tab1.setOnClosed(event -> ((Rectangle)tab3.getContent()).setFill(Color.RED));
        StackPane root = new StackPane();
        root.getChildren().add(tabs);

        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static List<StackPane> getSomething() {
        ArrayList<StackPane> list= new ArrayList<>();

        for(int i = 0; i< 10; i++) {

            StackPane stack = new StackPane();
            Rectangle rect = new Rectangle(64,32);
            rect.setFill(Color.WHITE);
            rect.setStroke(Color.BLACK);
            rect.setStrokeWidth(0.5);
            Text text = new Text(i + "");

            stack.getChildren().add(rect);
            stack.getChildren().add(text);
            list.add(stack);
        }

        return list;
    }

    public static List<StackPane> getSomething2() {
        ArrayList<StackPane> list= new ArrayList<>();

        for(int i = 0; i< 10; i++) {

            StackPane stack = new StackPane();
            Rectangle rect = new Rectangle(64,32);
            rect.setFill(Color.WHITE);
            rect.setStroke(Color.BLACK);
            rect.setStrokeWidth(0.5);
            Text text = new Text(i + "");
            stack.getChildren().add(rect);
            stack.getChildren().add(text);

            list.add(stack);
        }

        return list;
    }
}
