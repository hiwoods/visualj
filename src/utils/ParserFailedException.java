package utils;

/**
 * A general exception for program to report errors and crashes
 */
public class ParserFailedException extends Exception {
    public ParserFailedException(String message) {
        super(message);
    }
}