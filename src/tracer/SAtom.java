package tracer;

/**
 * things not surrounded by parenthesis. Ex: a
 */
public class SAtom extends SExpression {
    private String s;

    public SAtom(String s) {
        this.s = s;
    }

    public SAtom(int n) {
        this.s = Integer.toString(n);
    }

    public SAtom(long n) {this.s = Long.toString(n);}

    @Override
    public String toString() {
        return s;
    }
}
