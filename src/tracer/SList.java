package tracer;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.StringJoiner;

/**
 * SList: things surrounded by parenthesis. Ex: (a b c)
 */
public class SList extends SExpression {
    private SAtom operator;

    private LinkedList<SExpression> list = new LinkedList<SExpression>();

    public SList(String operator) {
        this.operator = new SAtom(operator);
        list.add(this.operator);
    }

    public void addLast(SExpression sexp) {
        list.add(sexp);
    }

    public SAtom getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(" ", "(", ")");
        for (SExpression sexp : list) {
            sj.add(sexp.toString());
        }
        return sj.toString();
    }
}
