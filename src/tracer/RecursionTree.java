package tracer;

import com.sun.jdi.LocalVariable;
import com.sun.jdi.Value;
import com.sun.org.apache.regexp.internal.RE;

import java.awt.*;
import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import java.util.ArrayList;

public class RecursionTree {
    private String funcName;
    private Node current;
    private Node root;
    private int i = 0;
    private final int initialColor = 255;
    private int decrement = 255;
    private int size = 0;
    private int exited_size = 0;

    class Node {
        List<String> arguments;
        String retval = "";
        Node parent;
        int color = -1;
        int stepID;
        boolean exited = false;
        ArrayList<Node> children = new ArrayList<>();

        Node(List<String> arguments, Node parent, int stepID) {
            this.arguments = arguments;
            this.parent = parent;
            this.stepID = stepID;
        }

        void addChild(List<String> arguments, int stepID) {
            Node child = new Node(arguments, this, stepID);
            children.add(child);
            current = child;
        }

        /**
         * get function name with arguments and return value
         * @return
         */
        String getLabel() {
            StringBuffer sb = new StringBuffer();
            sb.append(funcName);
            sb.append("(");
            for (String argument : arguments) {
                sb.append(argument + " ");
            }
            sb.deleteCharAt(sb.length()-1);
            sb.append(")");
            sb.append("="+retval);
            return sb.toString();
        }

        /**
         * Convert node to dot
         * @return
         */
        String toDot() {
            StringBuffer sb = new StringBuffer();
            String prefix = "node";
            String curr = prefix + i;
            for (Node child : children) {
                int new_color = initialColor - (initialColor/exited_size+1) * (initialColor - child.color);
                if (new_color < 16) {
                    new_color = 16;
                }
                if (!child.exited) {
                    new_color = 0;
                }
                i++;
                sb.append("\t" + prefix + i + " [label=\""+"@"+child.stepID+":"+child.getLabel()+"\"" + "labelfontcolor=\"white\" " + "style=filled color=\"#1E92FF\"" + " fillcolor=\"#" + Integer.toHexString(new_color) + "FFFF\""+"]\n");
                sb.append("\t" + curr + " -> " + prefix + i + "\n");
                sb.append(child.toDot());
            }
            return sb.toString();
        }

        /**
         * Convert node to SList
         * @return
         */
        SList getSList() {
            StringBuffer sb = new StringBuffer();
            sb.append(funcName);
            sb.append("(");
            for (String argument : arguments) {
                sb.append(argument + " ");
            }
            sb.deleteCharAt(sb.length()-1);
            sb.append(")");
            sb.append("="+retval);
            SList sList = new SList(sb.toString());

            for (Node child: children) {
                sList.addLast(child.getSList());
            }

            return sList;
        }


    }

    public RecursionTree(String funcName, List<String> arguments, int stepID) {
        this.funcName = funcName;
        current = new Node(arguments, null, stepID);
        root = current;
        ++size;
    }

    /**
     * Add a child to the tree
     * @param arguments
     * @param stepID
     */
    public void addChild(List<String> arguments, int stepID) {
        current.addChild(arguments, stepID);
        ++size;
    }

    /**
     * Mark a child as returned
     * @param retval
     */
    public void exitChild(String retval) {
        current.retval = retval;
        current.color = decrement;
        current.exited = true;
        ++exited_size;
        if (current.parent != null) {
            current = current.parent;
        }
        if (decrement > 0) {
            decrement--;
        }
    }

    public int getSize() {
        return size;
    }

    public String toString() {
        return current.getSList().toString();
    }

    /**
     * Check if all function calls in this recursion tree have been returned.
     * @return
     */
    public boolean isDone() {
        return size > 0 && size - exited_size == 0;
    }


    /**
     * Convert recursion tree to dot
     * @return
     */
    public String toDot() {
        if (size > 200) {
            return "digraph graphname {\n\tlabel=\"Warning: Over 200 recursive calls!\"\n}";
        }
        int new_color = initialColor - (initialColor/(exited_size+1)) * (initialColor - root.color);
        if (new_color < 16) {
            new_color = 16;
        }
        if (!root.exited) {
            new_color = 0;
        }
        String prefix = "node";
        StringBuffer sb = new StringBuffer();
        sb.append("digraph graphname {\n");
        sb.append("\tlabel=\"Total: " + size + "; Returned: " + exited_size + "; Not yet returned: " + (size - exited_size) + "\"\n\tfontsize=30\n");
        sb.append("\t" + prefix + i + " [label=\""+"@"+root.stepID+":"+root.getLabel()+"\"" + "labelfontcolor=\"white\" " + "style=filled color=\"#1E92FF\"" + " fillcolor=\"#" + Integer.toHexString(new_color) + "FFFF\""+"]\n");
        sb.append(root.toDot());
        sb.append("}\n");
        return sb.toString();
    }


    /**
     * Write the dot file and let the graphviz process it.
     * @throws Exception
     */
    public void writeDot() throws Exception {
        String filename = funcName + ".dot";
        String filename_out = funcName + ".png";
        PrintWriter printWriter = new PrintWriter(filename);
        printWriter.write(toDot());
        printWriter.close();
        try {
            String os = System.getProperty("os.name");
            ProcessBuilder pb;
            if (os.startsWith("Windows")) {
                pb = new ProcessBuilder("cmd.exe", "/C", "dot", "-Tpng", filename, "-o", filename_out);
                System.out.println("OS name is: " + os);
            } else {
                pb = new ProcessBuilder("dot", "-Tpng", filename, "-o", filename_out);
            }
            Process process = pb.start();
            int exitValue = process.waitFor();
            System.out.println("Exit value is " + exitValue);
            File file = new File(filename_out);
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
