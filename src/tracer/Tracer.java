package tracer;

import com.sun.jdi.*;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.event.*;
import com.sun.jdi.request.*;
import org.apache.commons.lang3.StringEscapeUtils;
import org.omg.CORBA.UserException;
import org.omg.PortableServer.THREAD_POLICY_ID;

import javax.tools.*;
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.zip.DataFormatException;

public class Tracer {
    private DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
    private VirtualMachine vm;
    private Process process;
    private EventRequestManager eventRequestManager;
    private EventQueue eventQueue;
    private EventSet eventSet;
    private boolean connected = true;
    private boolean waitingForInput = false;
    private String className;
    private String[] excludes = {"java.*", "javax.*", "sun.*", "com.sun.*"};
    private PrintWriter printWriter;
    private InputStream inputStream;
    private BufferedReader bufferedReader;
    private OutputStream outputStream;
    private BufferedWriter bufferedWriter;
    private String output = "";
    private int stepID = -1; //start at zero
    private SList interface_sl;
    private HashSet<Long> ids;
    private HashMap<String, RecursionTree> recursionTrees = new HashMap<>();
    private int frameNumber = 1;

    /**
     * Run the java code in debug mode.
     * @param src java source file
     * @throws Exception when syntax errors are found.
     */
    public Tracer(File src) throws Exception {
        File[] files = {src};
        List<String> options = new ArrayList<>();
        options.add("-g");
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
        boolean success = compiler.getTask(null, fileManager, diagnostics, options, null, compilationUnits).call();
        if (success) {
            String fileName = src.getName();
            className = fileName.substring(0, fileName.length() - 5);
            System.out.println("Class " + className + " is Compiled");

            LaunchingConnector launchingConnector = Bootstrap.virtualMachineManager().defaultConnector();
            Map<String, Connector.Argument> defaultArguments = launchingConnector.defaultArguments();
            Connector.Argument mainArg = defaultArguments.get("main");
            mainArg.setValue(className);
            Connector.Argument suspendArg = defaultArguments.get("suspend");
            suspendArg.setValue("true");
            vm = launchingConnector.launch(defaultArguments);
            process = vm.process();
            printWriter = new PrintWriter(className + ".txt");
            inputStream = process.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 1);
            eventRequestManager = vm.eventRequestManager();

            outputStream = process.getOutputStream();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
            setEventRequests();

            eventQueue = vm.eventQueue();
        } else {
            // TODO: 4/11/17
            Diagnostic<? extends JavaFileObject> diag = diagnostics.getDiagnostics().get(0);
            System.out.println("Class " + className + " is NOT Compiled");
            throw new DataFormatException(diag.getLineNumber() + " " + diag.getMessage(null));
        }
    }

    /**
     * Try to get n steps.
     *
     * @param n steps
     * @return number of actually got steps
     * @throws Exception
     */
    public int getSteps(int n) throws Exception {
        printWriter = new PrintWriter(printWriter, true);
        int oldStepID = stepID;
        while (connected && stepID < oldStepID + n) {
            eventSet = eventQueue.remove(1000);
            if (eventSet == null) {
                waitingForInput = true;
                break;
            } else {
                waitingForInput = false;
                for (Event event : eventSet) {
                    handle(event);
                    eventSet.resume();
                }
            }
        }
        for (Map.Entry<String, RecursionTree> entry : recursionTrees.entrySet()) {
            String key = entry.getKey();
            RecursionTree value = entry.getValue();
            System.out.println("Recursion Tree: " + key);
            System.out.println(value);
            System.out.println(value.toDot());
            if (value.getSize() > 1) {
                value.writeDot();
            }
        }
        return stepID - oldStepID;
    }

    /**
     * Try to get n steps with user input.
     * @param n steps
     * @param s user input
     * @return number of actually got steps
     * @throws Exception
     */
    public int getSteps(int n, String s) throws Exception {
        bufferedWriter.write(s, 0, s.length());
        bufferedWriter.write("\n");
        bufferedWriter.flush();
        printWriter = new PrintWriter(printWriter, true);
        int oldStepID = stepID;
        while (connected && stepID < oldStepID + n) {
            eventSet = eventQueue.remove(1000);
            if (eventSet == null) {
                waitingForInput = true;
                break;
            } else {
                waitingForInput = false;
                for (Event event : eventSet) {
                    handle(event);
                    eventSet.resume();
                }
            }
        }
        return stepID - oldStepID;
    }

    /**
     * Check if the program is still running
     * @return
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * check if the program is waiting for user input
     * @return
     */
    public boolean isWaitingForInput() {
        return waitingForInput;
    }

    /**
     * Tracer should be closed when you are done, no matter the program has finished or not.
     * Thus, this method can also force terminating a program.
     *
     * @throws Exception
     */
    public void close() throws Exception {
        outputStream.close();
        bufferedWriter.close();
        printWriter.close();
        inputStream.close();
        bufferedReader.close();
        process.destroy();
        if (connected) {
            vm.exit(1);
            connected = false;
            System.out.println("Tracer is Forcibly Closed");
        } else {
            System.out.println("Tracer is Closed");
        }
    }

    /**
     * Request debug events
     */
    private void setEventRequests() {
        ClassPrepareRequest classPrepareRequest = eventRequestManager.createClassPrepareRequest();
        classPrepareRequest.addClassFilter(className);
        classPrepareRequest.addCountFilter(1);
        classPrepareRequest.setSuspendPolicy(EventRequest.SUSPEND_ALL);
        classPrepareRequest.enable();

        MethodExitRequest methodExitRequest = eventRequestManager.createMethodExitRequest();
        methodExitRequest.setSuspendPolicy(EventRequest.SUSPEND_ALL);
        for (String exclude : excludes) {
            methodExitRequest.addClassExclusionFilter(exclude);
        }
        methodExitRequest.enable();

        ExceptionRequest exceptionRequest = eventRequestManager.createExceptionRequest(null, false, true);
        exceptionRequest.setSuspendPolicy(EventRequest.SUSPEND_ALL);
        exceptionRequest.enable();
    }

    /**
     * Get arguments of a method
     * @param threadReference the thread the program is running on
     * @param methodName the name of the method defined in the program
     * @return arguments
     * @throws IncompatibleThreadStateException
     */
    private List<String> getArguments(ThreadReference threadReference, String methodName) throws IncompatibleThreadStateException {
        List<String> values = new ArrayList<>();
        StackFrame stackFrame = threadReference.frame(0);
        for (Value value : stackFrame.getArgumentValues()) {
            values.add(value.toString());
        }
        return values;
    }

    /**
     * Handle debug event
     * @param event
     * @throws Exception
     */
    private void handle(Event event) throws Exception {
        if (event instanceof VMStartEvent) {
            System.out.println("VM Started");
        } else if (event instanceof ClassPrepareEvent) {
            ClassPrepareEvent classPrepareEvent = (ClassPrepareEvent) event;
            String mainClassName = classPrepareEvent.referenceType().name();
            if (mainClassName.equals(className)) {
                System.out.println("Class " + mainClassName + " is Prepared");
                ReferenceType referenceType = classPrepareEvent.referenceType();
                List<Location> locations = referenceType.allLineLocations();
                locations.remove(0);
                for (Location location : locations) {
                    BreakpointRequest breakpointRequest = eventRequestManager.createBreakpointRequest(location);
                    breakpointRequest.setSuspendPolicy(EventRequest.SUSPEND_ALL);
                    breakpointRequest.enable();
                }
            }
        } else if (event instanceof VMDisconnectEvent) {
            connected = false;
            System.out.println("Program is Finished");
            for (Map.Entry<String, RecursionTree> entry : recursionTrees.entrySet()) {
                String key = entry.getKey();
                RecursionTree value = entry.getValue();
                System.out.println("Recursion Tree: " + key);
                System.out.println(value);
                System.out.println(value.toDot());
                if (value.getSize() > 1) {
                    value.writeDot();
                }
            }
        } else if (event instanceof LocatableEvent) {
            ThreadReference threadReference = ((LocatableEvent) event).thread();
            threadReference.suspend();

            SList step = new SList("step");
            step.addLast(new SAtom(++stepID));
            SList line = new SList("line");
            step.addLast(line);
            Location location = ((LocatableEvent) event).location();
            int lineNumber = location.lineNumber();
            line.addLast(new SAtom(lineNumber));

            SList stack = new SList("stack");
            step.addLast(stack);
            interface_sl = new SList("interface");
            step.addLast(interface_sl);
            SList except = new SList("exception");
            step.addLast(except);

            ids = new HashSet<>();

            List<StackFrame> stackFrames = threadReference.frames();
            Deque<StackFrame> stk = new ArrayDeque<StackFrame>();
            for (StackFrame stackFrame : stackFrames) {
                if (interestedFrame(stackFrame)) {
                    stk.push(stackFrame);
                }
            }
            if (stk.size() > frameNumber) {
                String methodName = stackFrames.get(0).location().method().name();
                String previousMethodName = null;
                if (stk.size() >= 2) {
                    previousMethodName = stackFrames.get(1).location().method().name();
                }
                if (!methodName.equals("main") && !methodName.startsWith("<") && !methodName.endsWith(">")) {
                    if (!recursionTrees.containsKey(methodName)) {
                        recursionTrees.put(methodName, new RecursionTree(methodName, getArguments(threadReference, methodName), stepID));
                    } else if (previousMethodName != null && previousMethodName.equals(methodName)) {
                        recursionTrees.get(methodName).addChild(getArguments(threadReference, methodName), stepID);
                    }
                    frameNumber = stk.size();
                }
            }

            for (StackFrame stackFrame : stk) {
                Location frameLocation = stackFrame.location();
                SList frame = new SList("frame");
                Method method = frameLocation.method();
                String frameName = method.name();
                frame.addLast(new SAtom(frameName));
                stack.addLast(frame);

                for (LocalVariable localVariable : method.variables()) {
                    if (localVariable.isVisible(stackFrame) &&
                            !(frameName.equals("main") && localVariable.name().equals("args"))) {
                        Value value = stackFrame.getValue(localVariable);
                        SList bind = new SList("bind");
                        String localName = localVariable.name() + ":" + getType(localVariable.typeName());
                        bind.addLast(new SAtom(localName));
                        bind.addLast(processValue(value));
                        frame.addLast(bind);
                    }
                }
                if (event instanceof MethodExitEvent && stackFrame.equals(stackFrames.get(0))) {
                    SList ret = new SList("ret");
                    Value retval = ((MethodExitEvent) event).returnValue();
                    ret.addLast(processValue(retval));
                    frame.addLast(ret);
                    if (!frameName.equals("main") && !frameName.startsWith("<") && !frameName.endsWith(">")) {
                        if (recursionTrees.containsKey(frameName)) {
                            recursionTrees.get(frameName).exitChild(retval.toString());
                            frameNumber -= 1;
                        }
                    }

                    ArrayList<String> toBeRemoved = new ArrayList<>();

                    for (Map.Entry<String, RecursionTree> entry : recursionTrees.entrySet()) {
                        String key = entry.getKey();
                        RecursionTree value = entry.getValue();
                        if (value.isDone()) {
                            toBeRemoved.add(key);
                            if (value.getSize() > 1) {
                                value.writeDot();
                            }
                        }
                    }

                    for (String beRemoved : toBeRemoved) {
                        recursionTrees.remove(beRemoved);
                    }
                }
            }

            if (event instanceof ExceptionEvent) {

                ObjectReference objectReference = ((ExceptionEvent) event).exception();
                ReferenceType referenceType = objectReference.referenceType();
                Field field = referenceType.fieldByName("detailMessage");
                except.addLast(new SAtom(referenceType.name()));
                System.out.println(referenceType.name());
                Value message = objectReference.getValue(field);
                if (message == null) {
                    except.addLast(new SAtom("\"\""));
                } else {
                    except.addLast(new SAtom(message.toString()));
                }
//                System.out.println(except.toString());
            }


            SList stdout = new SList("stdout");
            int buffer;
            while (bufferedReader.ready() && (buffer = bufferedReader.read()) != -1) {
                char buf = (char) buffer;
                if (buf == '\n') {
                    output += "\\n";
                } else if (buf == '\r') {
                    output += "\\r";
                } else {
                    output += (char) buffer;
                }
            }
            if (output != "") {
                stdout.addLast(new SAtom(output));
            }
            step.addLast(stdout);

            printWriter.println(step);
            threadReference.resume();

        } else {

        }
    }

    /**
     * Filter stack frames not defined in the program
     * @param frame
     * @return
     */
    private boolean interestedFrame(StackFrame frame) {
        Location location = frame.location();
        return !excluded(location.toString());
    }

    /**
     * filter classes
     * @param className
     * @return
     */
    private boolean excluded(String className) {
        for (String exclude : excludes) {
            int index = exclude.indexOf('.');
            String newExclude = exclude.substring(0, index) + ".";
            if (className.startsWith(newExclude)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Convert the values of variables to SExpressions
     * @param value
     * @return
     * @throws ClassNotLoadedException
     */
    private SExpression processValue(Value value) throws ClassNotLoadedException {
        if (value instanceof PrimitiveValue) {
            if (value instanceof CharValue) {
                return new SAtom("'" + StringEscapeUtils.escapeJava(value.toString()) + "'");
            } else if (value instanceof VoidValue) {
                return new SAtom("void");
            } else {
                return new SAtom(value.toString());
            }
        } else if (value instanceof ObjectReference) {
            long id = ((ObjectReference) value).uniqueID();
            ReferenceType referenceType = ((ObjectReference) value).referenceType();
            String typeName = referenceType.name();
            String lang = "java.lang.";
            String util = "java.util.";
            final String[] wrappers = {"Boolean", "Character", "Byte", "Short", "Integer", "Long", "Float", "Double"};
            if (value instanceof StringReference) {
                if (ids.contains(id)) {
                    SList sList = new SList("ref");
                    sList.addLast(new SAtom(id));
                    return sList;
                } else {
                    ids.add(id);
                }
                SList string = new SList("String");
                String content = "\"" + StringEscapeUtils.escapeJava(((StringReference) value).value()) + "\"";
                string.addLast(new SAtom(id));
                string.addLast(new SAtom(content));
                return string;
            } else if (value instanceof ArrayReference) {
                if (ids.contains(id)) {
                    SList sList = new SList("ref");
                    sList.addLast(new SAtom(id));
                    return sList;
                } else {
                    ids.add(id);
                }
                SList array = new SList("Array");
                array.addLast(new SAtom(id));
                for (Value v : ((ArrayReference) value).getValues()) {
                    array.addLast(processValue(v));
                }
                return array;
            } else if (typeName.startsWith(lang)) {
                String suffix = typeName.substring(lang.length());
                if (Arrays.asList(wrappers).contains(suffix)) {
                    Field field = referenceType.fieldByName("value");
                    if (suffix.equals("Character")) {
                        return new SAtom("'" + ((ObjectReference) value).getValue(field).toString() + "'");
                    } else {
                        return new SAtom(((ObjectReference) value).getValue(field).toString());
                    }
                } else {
                    return new SList("ref");
                }
            } else if (typeName.startsWith(util)) {
                if (ids.contains(id)) {
                    SList sList = new SList("ref");
                    sList.addLast(new SAtom(id));
                    return sList;
                } else {
                    ids.add(id);
                }
                // TODO: 10/31/17 filter suffix: no dot
                boolean isCollection = false;
                String suffix = typeName.substring(util.length());
                SList class_sl = new SList(suffix);
                if (!suffix.contains("$")) {
                    ClassType classType = (ClassType) ((ObjectReference) value).referenceType();
                    for (InterfaceType it : classType.allInterfaces()) {
                        if (it.name().startsWith(util)) {
                            class_sl.addLast(new SAtom(it.name().substring(util.length())));
                        }
                        if (it.name().equals("java.util.Collection")) {
                            isCollection = true;
                        }
                    }
                } else {
                    isCollection = true;
                }
                if (isCollection) {
                    interface_sl.addLast(class_sl);
                    if (suffix.equals("ArrayList")) {
                        Field size = referenceType.fieldByName("size");
                        Field elementData = referenceType.fieldByName("elementData");
                        SList arrayList = new SList("ArrayList");
                        arrayList.addLast(new SAtom(id));
                        SList size_l = new SList("size:" + getType(size.typeName()) + "=" + ((ObjectReference) value).getValue(size).toString());
                        SList elementData_l = new SList("elementData:" + getType(elementData.typeName()) + "=" + processValue(((ObjectReference) value).getValue(elementData)));
                        arrayList.addLast(size_l);
                        arrayList.addLast(elementData_l);
                        return arrayList;
                    } else {
                        SList slist = new SList(suffix);
                        slist.addLast(new SAtom(id));
                        for (Field field : referenceType.visibleFields()) {
                            if (interestedField(field)) {
                                Value field_v = ((ObjectReference) value).getValue(field);
                                SList field_l = new SList(field.name() + ":" + getType(field.typeName()) + "=" + processValue(field_v));
                                slist.addLast(field_l);
                            }
                        }
                        return slist;
                    }
                } else {
                    return new SList("ref");
                }

            } else if (typeName.equals(className) || typeName.startsWith(className + "$")) {
                if (ids.contains(id)) {
                    SList sList = new SList("ref");
                    sList.addLast(new SAtom(id));
                    return sList;
                } else {
                    ids.add(id);
                }
                SList slist = new SList(className);
                slist.addLast(new SAtom(id));
                for (Field field : referenceType.visibleFields()) {
                    if (interestedField(field)) {
                        Value field_v = ((ObjectReference) value).getValue(field);
                        SList field_l = new SList(field.name() + ":" + getType(field.typeName()) + "=" + processValue(field_v));
                        slist.addLast(field_l);
                    }
                }
                return slist;
            } else {
                return new SList("ref");
            }
        } else if (value instanceof VoidValue) {
            return new SAtom("void");
        } else {
            return new SAtom("null");
        }
    }

    /**
     * filter fields that may not be useful to users
     * @param field
     * @return
     */
    private boolean interestedField(Field field) {
        return !field.isStatic() && !field.name().equals("modCount") ||
                field.name().equals("elementData") ||
                field.name().equals("size");
    }

    private String getType(String typeName) {
        String lang = "java.lang.";
        String util = "java.util.";
        int index = typeName.indexOf("[]");
        if (index != -1) {
            if (typeName.startsWith(lang)) {
                return typeName.substring(lang.length(), index + 2);
            } else if (typeName.startsWith(util)) {
                return typeName.substring(util.length(), index + 2);
            } else {
                return typeName.substring(0, index + 2);
            }
        } else {
            if (typeName.startsWith(lang)) {
                return typeName.substring(lang.length());
            } else if (typeName.startsWith(util)) {
                return typeName.substring(util.length());
            } else {
                return typeName;
            }
        }
    }
}
