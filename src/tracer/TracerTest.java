package tracer;

import java.io.File;

public class TracerTest {
    public static void main(String[] args) throws Exception {
        File src = new File("Person.java");
        Tracer tracer = new Tracer(src);
        // when less steps got than requested, it means tracer is terminated or is waiting for input.
        while (tracer.isConnected()) {
            if (tracer.isWaitingForInput()) {
                // when tracer is waiting for input, user should be promoted to input a string
                // After inputting a string, if tracer is still waiting for input, user will be promoted again
                // In this case, "2" has been inputted twice
                tracer.getSteps(50, "2"); // actually inputted string is "2\n". \n is appended by the tracer since it's always required.
            } else {
                tracer.getSteps(50);
            }
        }
//        int n = -1;
//        while (n != 0) {
//            // Try to get 107 steps
//            // n is the number of actually got steps
//            // if n is 0, the program finishes
//            n = tracer.getSteps(107);
//            System.out.println(n);
//        }
//        tracer.close();
    }
}
