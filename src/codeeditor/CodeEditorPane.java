package codeeditor;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.fxmisc.wellbehaved.event.EventPattern;
import org.fxmisc.wellbehaved.event.InputMap;
import org.fxmisc.wellbehaved.event.Nodes;
import org.reactfx.value.Val;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.IntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple code editor with basic syntax highlighting
 * <p>
 * Reference: https://github.com/FXMisc/RichTextFX
 * Adapted and modified by Lam Cao on 11/23/2017.
 */
public class CodeEditorPane extends StackPane {

    private static final String[] KEYWORDS = new String[]{
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "final", "finally", "float",
            "for", "goto", "if", "implements", "import",
            "instanceof", "int", "interface", "long", "native",
            "new", "package", "private", "protected", "public",
            "return", "short", "static", "strictfp", "super",
            "switch", "synchronized", "this", "throw", "throws",
            "transient", "try", "void", "volatile", "while"
    };

    private static final String KEYWORD_PATTERN;
    private static final String PAREN_PATTERN;
    private static final String BRACE_PATTERN;
    private static final String BRACKET_PATTERN;
    private static final String SEMICOLON_PATTERN;
    private static final String STRING_PATTERN;
    private static final String COMMENT_PATTERN;

    static {
        SEMICOLON_PATTERN = ";";
        PAREN_PATTERN = "\\(|\\)";
        BRACE_PATTERN = "\\{|}";
        BRACKET_PATTERN = "\\[|]";
        STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
        COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";
        KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    }

    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<PAREN>" + PAREN_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

    private static final String TAB = "    ";
    private static final String sampleCode = String.join("\n", new String[]{
            "public class Main {",
            "",
            "    /*",
            "     * A Hello-World example",
            "     */",
            "    public static void main(String[] args) {",
            "        System.out.println(\"Hello World\");",
            "    }",
            "",
            "}"
    });

    private CodeArea codeArea;
    private ExecutorService executor;
    private IntegerProperty currentStepLineProperty;

    public CodeEditorPane() {
        currentStepLineProperty = new SimpleIntegerProperty(-1);
        ObservableValue<Integer> currentStepLineObservable = currentStepLineProperty.asObject();
        executor = Executors.newSingleThreadExecutor();
        codeArea = new CodeArea();
        //set up number and arrow columns
        IntFunction<Node> numberFactory = LineNumberFactory.get(codeArea);
        IntFunction<Node> arrowFactory = new ArrowFactory(currentStepLineObservable);
        IntFunction<Node> graphicFactory = line -> {
            HBox hbox = new HBox(
                    numberFactory.apply(line),
                    arrowFactory.apply(line));
            hbox.setAlignment(Pos.CENTER_LEFT);
            return hbox;
        };
        codeArea.setParagraphGraphicFactory(graphicFactory);

        //set up event on text changed
        codeArea.richChanges()
                .filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
                .successionEnds(Duration.ofMillis(500))
                .supplyTask(this::computeHighlightingAsync)
                .awaitLatest(codeArea.richChanges())
                .filterMap(t -> {
                    if (t.isSuccess()) {
                        return Optional.of(t.get());
                    } else {
                        t.getFailure().printStackTrace();
                        return Optional.empty();
                    }
                })
                .subscribe(this::applyHighlighting);
        codeArea.replaceText(0, 0, sampleCode);
        this.getChildren().add(new VirtualizedScrollPane<>(codeArea));
        this.getStylesheets().add(CodeEditorPane.class.getResource("java-keywords.css").toExternalForm());

        //replace 8-space tab with 4-space
        Nodes.addInputMap(codeArea, InputMap.sequence(
                InputMap.consume(EventPattern.keyPressed(KeyCode.TAB), e -> codeArea.replaceSelection(TAB))
        ));
        //highlight current line
        currentStepLineProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                codeArea.setParagraphStyle(newValue.intValue() - 1, Collections.singleton("debug"));
                codeArea.showParagraphInViewport(newValue.intValue() - 1);
            }
            if (oldValue.intValue() != -1)
                codeArea.clearParagraphStyle(oldValue.intValue() - 1);
        });
    }

    public void setSourceCode(String text) {
        codeArea.replaceText(text);
    }

    public String getSourceCode() {
        return codeArea.getText();
    }

    public void setArrowOnLine(int line) {
        currentStepLineProperty.set(line);
    }

    private Task<StyleSpans<Collection<String>>> computeHighlightingAsync() {
        String text = codeArea.getText();
        Task<StyleSpans<Collection<String>>> task = new Task<StyleSpans<Collection<String>>>() {
            @Override
            protected StyleSpans<Collection<String>> call() throws Exception {
                return computeHighlighting(text);
            }
        };
        executor.execute(task);
        return task;
    }

    private void applyHighlighting(StyleSpans<Collection<String>> highlighting) {
        codeArea.setStyleSpans(0, highlighting);
    }

    private static StyleSpans<Collection<String>> computeHighlighting(String text) {
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                            matcher.group("PAREN") != null ? "paren" :
                                    matcher.group("BRACE") != null ? "brace" :
                                            matcher.group("BRACKET") != null ? "bracket" :
                                                    matcher.group("SEMICOLON") != null ? "semicolon" :
                                                            matcher.group("STRING") != null ? "string" :
                                                                    matcher.group("COMMENT") != null ? "comment" :
                                                                            null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

    class ArrowFactory implements IntFunction<Node> {
        private final ObservableValue<Integer> shownLine;

        ArrowFactory(ObservableValue<Integer> shownLine) {
            this.shownLine = shownLine;
        }

        @Override
        public Node apply(int lineNumber) {
            Polygon triangle = new Polygon(0.0, 0.0, 10.0, 5.0, 0.0, 10.0);
            triangle.setFill(Color.GREEN);

            ObservableValue<Boolean> visible = Val.map(
                    shownLine,
                    sl -> sl - 1 == lineNumber);

            triangle.visibleProperty().bind(
                    Val.flatMap(triangle.sceneProperty(), scene -> scene != null ? visible : Val.constant(false)));
            return triangle;
        }
    }

    public void setEditable(boolean state) {
        codeArea.setEditable(state);
    }
}