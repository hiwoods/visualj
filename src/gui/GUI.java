package gui;/**
 * Created by Hiwoods on 3/21/2017.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;

public class GUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/gui/gui.fxml"));
        primaryStage.setTitle("VisualJ");
        primaryStage.setScene(new Scene(root, 900, 600));
        primaryStage.show();

    }
}
