package gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.scene.control.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.control.Slider;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.io.*;

import codeeditor.CodeEditorPane;

import parser.Instruction;
import parser.StepManager;
import utils.DraggableTab;
import utils.InvalidCodeException;
import visualizer.VisualizerUI;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.*;

/**
 * Main GUIController
 * Created by Hiwoods on 3/22/2017.
 */
public class GuiController implements Initializable {

    @FXML
    private CodeEditorPane codeArea;
    @FXML
    private TextArea console;
    @FXML
    private TextField stepBox;
    @FXML
    private Label stepBoxMaxLabel;
    @FXML
    private Slider slider;
    @FXML
    private Button runButton;
    @FXML
    private Button stepBackButton;
    @FXML
    private Button stepForwardButton;
    @FXML
    private TabPane visualizerTabPane;

    private ArrayList<VisualizerUI> visualizers;
    private File file;
    private StepManager stepManager;
    private final Label labelFile = new Label();
    private boolean debuggerStarted;
    private Image imagePause;
    private Image imageRun;
    private ImageView runBtnImgView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //----------------------Slider---------------------------------
        setUpSlider();
        //----------------------Step Box-------------------------------
        setUpStepBox();
        //disable all debugging buttons
        setButtonsDisabled(true);
        //run button images
        imagePause = new Image("pause.png");
        imageRun = new Image("run.jpg");
        runBtnImgView = new ImageView(imageRun);
        runButton.setGraphic(runBtnImgView);
        runBtnImgView.setFitWidth(25);
        runBtnImgView.setFitHeight(25);
        debuggerStarted = false;

        visualizers = new ArrayList<>();
        VisualizerUI defaultUI = new VisualizerUI();
        visualizers.add(defaultUI);
        DraggableTab defaultTab = new DraggableTab("default");
        defaultTab.setContent(defaultUI);

        visualizerTabPane.getTabs().add(defaultTab);
    }

    public void onRunButtonClicked() throws Exception {
        String usersCode = codeArea.getSourceCode();
        if (usersCode.equals("")) {
            console.setText("Code is empty");
            return;
        }

        Pattern classDeclarationPattern = Pattern.compile(".*(?:public|private)?\\s+class\\s+(\\w+)\\s*((?:extends\\s+\\w+\\s*)|(?:implements\\s+[,\\w\\s]+\\s*))*\\s*\\{");
        Matcher m = classDeclarationPattern.matcher(usersCode);
        String sourceFileName = null;
        if (m.find()) {
            sourceFileName = m.group(1);
        }

        try {
            file = new File(sourceFileName + ".java");
            FileWriter writer = new FileWriter(file);
            writer.write(usersCode);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //-----------------------Important-------------------------------
        //----------------------Set up StepManager-------------------

        //if true, terminate the the session.
        if (debuggerStarted) {
            debuggerStarted = false;
            runBtnImgView.setImage(imageRun);
            codeArea.setArrowOnLine(-1);
            setButtonsDisabled(true);
            stepBox.setText("0");
            stepBoxMaxLabel.setText("0");
            console.setText("");
            codeArea.setEditable(true);
            for (VisualizerUI ui : visualizers) {
                ui.reset();
            }
            return;
        } else {
            try {
                stepManager = new StepManager(file);
            } catch (InvalidCodeException e) {
                String message = e.getMessage();
                console.setText(message);
                return;
            }
        }

        //--------------Start new session--------------------------
        debuggerStarted = true;
        codeArea.setEditable(false);
        runBtnImgView.setImage(imagePause);
        setButtonsDisabled(false);
        for (VisualizerUI ui : visualizers) {
            stepManager.addObserver(ui);
        }
        //-------------------change range of slider and set box----------
        int lastStepID = stepManager.getLastInstructionID();
        slider.adjustValue(0);
        slider.setMax(lastStepID);
        stepBox.setText(stepManager.getFirstInstructionID() + "");
        stepBoxMaxLabel.setText("/" + lastStepID);
        codeArea.setArrowOnLine(stepManager.jumpTo(0).getLineNumber());
    }

    /**
     * Set up StepBox
     */
    private void setUpStepBox() {
        stepBox.setText("0");
        stepBox.focusedProperty().addListener((obs, oldVal, newVal) -> {
            //if lost focus
            if (!newVal && oldVal) {
                int newIndex = Integer.parseInt(stepBox.getText());
                try {
                    slider.adjustValue(newIndex);
                } catch (NumberFormatException e) {
                    int firstStepID = stepManager.getFirstInstructionID();
                    stepBox.setText("" + firstStepID);
                    slider.setValue(firstStepID);
                }
            }
        });
        stepBox.setOnKeyReleased(event1 -> {
            if (event1.getCode() == KeyCode.ENTER) {
                //visualizerUI.requestFocus();
                visualizerTabPane.requestFocus();
            }
        });
    }

    /**
     * Set up slider
     */
    private void setUpSlider() {
        slider.setMin(0);
        slider.adjustValue(-1);
        slider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            int newStepIndex = (int) newValue.doubleValue();
            //set the number box, max values
            stepBox.setText(newStepIndex + "");
            slider.setDisable(true);
            //get the steps
            Instruction instr = stepManager.jumpTo(newStepIndex);

            if (slider.getMax() != stepManager.getLastInstructionID()) {
                slider.setMax(stepManager.getLastInstructionID());
                stepBoxMaxLabel.setText("/" + stepManager.getLastInstructionID());
                //visualizerUI.requestFocus();
                visualizerTabPane.requestFocus();
            }
            if (instr.getStdOutPut() != null) {
                String a = instr.getStdOutPut();
                a = a.replaceAll("\\\\r?\\\\n", "\n");
                console.setText(a);
            }

            if (instr.getException() != null) {
                console.setText(console.getText() + instr.getException().toString());
            } else {
                //change arrow location to match line number
                codeArea.setArrowOnLine(instr.getLineNumber());
            }
            slider.setDisable(false);
        });
    }


    public void onStepBackClicked() {
        slider.decrement();
    }

    public void onStepForwardClicked() {
        slider.increment();
    }

    public void clo() {
        System.exit(0);
    }

    public void pressMenuFileNew() {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResource("/readonly/Main.java").openStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        codeArea.setSourceCode(sb.toString());
    }

    public void pressMenuOpen() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAVA files (*.java)", "*.java");
        fileChooser.getExtensionFilters().add(extFilter);
        //Show open file dialog
        file = fileChooser.showOpenDialog(null);
        if (file != null) {
            labelFile.setText(file.getPath());
            codeArea.setSourceCode(readFile(file));
        }
    }

    public void pressMenuSave() {
        FileChooser fileChooser1 = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAVA files (*.java)", "*.java");
        fileChooser1.getExtensionFilters().add(extFilter);
        fileChooser1.setTitle("Saving file");
        file = fileChooser1.showSaveDialog(null);
        if (file != null) {
            try {
                saveFileRoutine(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void pressHelp() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Presnet");
        alert.setHeaderText("Look, an cs4500 class");
        alert.setContentText("Ooops, what am i doing!");
        alert.showAndWait();
    }

    public void pressMenuSaveAs() {
        FileChooser fileChooser1 = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAVA files (*.java)", "*.java");
        fileChooser1.getExtensionFilters().add(extFilter);
        fileChooser1.setTitle("Saving file");
        file = fileChooser1.showSaveDialog(null);
        if (file != null) {
            try {
                saveFileRoutine(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static String readFile(File selectedFile) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            FileReader fr = new FileReader(selectedFile);
            BufferedReader br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return sb.toString();
    }

    private void setButtonsDisabled(boolean state) {
        stepBox.setDisable(state);
        slider.setDisable(state);
        stepBackButton.setDisable(state);
        stepForwardButton.setDisable(state);
    }

    private void saveFileRoutine(File file) throws IOException {
        // Creates a new file and writes the txtArea contents into it
        String txt = codeArea.getSourceCode();
        if (!file.createNewFile())
            throw new IOException("Cannot create file" + file.getName());
        FileWriter writer = new FileWriter(file);
        writer.write(txt);
        writer.close();
    }

    public void onNewVisualizerTabClicked() {
        DraggableTab tab = new DraggableTab("new tab");
        VisualizerUI ui = new VisualizerUI();
        visualizers.add(ui);
        tab.setContent(ui);
        if (stepManager != null)
            stepManager.addObserver(ui);
        visualizerTabPane.getTabs().add(tab);
    }
}
