package visualizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import parser.Instruction;
import parser.VObject;
import parser.VStack;

import java.util.List;

/**
 * Model for VisualizerUI
 * Created by Lam Cao on 11/28/2017.
 */
class VisualizerModel extends java.util.Observable {

    private Instruction currentInstruction;
    private ObservableList<VStack> stacks;
    private ObservableList<VObject> vObjects;

    VisualizerModel() {
        stacks = FXCollections.observableArrayList();
        vObjects = FXCollections.observableArrayList();
    }

    void setInstructionData(Instruction instr) {
        resetData();
        currentInstruction = instr;
        addStackListInReversedOrder(instr.getListOfVStack());
        vObjects.addAll(instr.getListOfVObject());
        setChanged();
    }

    void resetData() {
        vObjects.clear();
        stacks.clear();
    }

    private void addStackListInReversedOrder(List<VStack> list) {
        stacks.addAll(list);
    }

    ObservableList<VStack> getStacks() {
        return stacks;
    }

    ObservableList<VObject> getvObjects() {
        return vObjects;
    }

    Instruction getCurrentInstruction() {
        return currentInstruction;
    }

}
