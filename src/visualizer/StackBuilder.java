package visualizer;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import parser.Instruction;
import parser.VStack;
import parser.VVariable;
import utils.GridBox;
import utils.TitledAnchorPane;

import java.util.List;

import static utils.GridBox.DEFAULT_CELL_HIGHLIGHT_COLOR;

/**
 * Create new and update stack nodes to be drawn on canvas.
 * <p>
 * Created by Lam Cao
 *
 * @see visualizer.NodeBuilder
 */
public class StackBuilder implements NodeBuilder {

    @Override
    public VNode makeNode(int index, Instruction instruction) {
        VStack stack = instruction.getListOfVStack().get(index);
        double maxWidth = computeNeededWidth(stack.getVariables());

        GridBox varList = new GridBox(maxWidth, GridBox.DEFAULT_CELL_HEIGHT, 1, 1);
        for (VVariable var : stack.getVariables()) {
            StackPane pane = new StackPane();
            Rectangle rect = varList.makeRectangle(varList.getMaxCellWidth(), GridBox.DEFAULT_CELL_HEIGHT, DEFAULT_CELL_HIGHLIGHT_COLOR, true);
            pane.getChildren().add(rect);
            pane.getChildren().add(new Label(var.toString()));
            varList.addRows(pane);
        }

        TitledAnchorPane container = new TitledAnchorPane();
        container.showBorder(true);
        container.setTitleLabel(stack.getStackName());
        container.getContainerPane().getChildren().add(varList);

        return new VNode(container, index, true);
    }

    @Override
    public VNode updateNode(VNode oldNode, int index, Instruction instruction) {
        VStack stack = instruction.getListOfVStack().get(index);
        TitledAnchorPane titledPane = oldNode.getContainerPane();
        GridBox content = (GridBox) titledPane.getContainerPane().getChildren().get(0);
        double newWidth = computeNeededWidth(stack.getVariables());
        content.setMaxCellWidth(newWidth);

        int limit = content.getNumRow() >= stack.getVariables().size() ? stack.getVariables().size() : content.getNumRow();
        boolean removeRest = content.getNumRow() > stack.getVariables().size();
        int col = 0;
        int i = 0;
        for (; i < limit; i++) {
            VVariable var = stack.getVariables().get(i);
            HBox row = (HBox) content.getChildren().get(i);
            StackPane pane = (StackPane) row.getChildren().get(col);
            Rectangle rect = (Rectangle) pane.getChildren().get(0);
            Label contentLabel = (Label) pane.getChildren().get(1);

            if (contentLabel.getText().equals(var.toString())) {
                rect.setFill(Color.WHITE);
            } else {
                contentLabel.setText(var.toString());
                rect.setFill(DEFAULT_CELL_HIGHLIGHT_COLOR);
            }
        }
        if (removeRest) {
            for (; i < content.getNumRow(); i++) {
                content.removeRow(content.getChildren().size() - 1);
            }
        } else {
            for (; i < stack.getVariables().size(); i++) {
                StackPane pane = new StackPane();
                Rectangle rect = content.makeRectangle(content.getMaxCellWidth(), GridBox.DEFAULT_CELL_HEIGHT, DEFAULT_CELL_HIGHLIGHT_COLOR, true);
                pane.getChildren().add(rect);
                pane.getChildren().add(new Label(stack.getVariables().get(i).toString()));
                content.addRows(pane);
            }
        }
        content.refreshSize();
        return oldNode;
    }

    private double computeNeededWidth(List<VVariable> vars) {
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        double maxWidth = 0;
        for (VVariable var : vars) {
            Label label = new Label(var.toString());
            double width = Math.ceil(fontLoader.computeStringWidth(label.getText(), label.getFont()));
            if (width > maxWidth)
                maxWidth = width;
        }
        return maxWidth;
    }
}