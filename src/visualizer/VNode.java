package visualizer;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import parser.*;
import utils.ArrowLine;
import utils.GridBox;
import utils.TitledAnchorPane;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static utils.GridBox.DEFAULT_CELL_HEIGHT;

/**
 * VNode represents an object or stack to be drawn on canvas
 * <p>
 * Created by Lam Cao
 */
class VNode extends Group {
    private TitledAnchorPane containerPane;
    private boolean isStack;
    private LinkedList<ArrowLine> arrowOut;
    private LinkedList<ArrowLine> arrowIn;
    private int objectID;
    private boolean arrowOutVisible;
    private boolean arrowInVisible;

    @SuppressWarnings("unchecked")
    VNode(TitledAnchorPane node, int objID, boolean isStack) {
        super();
        this.containerPane = node;
        this.isStack = isStack;
        getChildren().add(this.containerPane);
        arrowOut = new LinkedList<>();
        arrowIn = new LinkedList<>();
        objectID = objID;
        this.isStack = isStack;
        this.setPickOnBounds(false);
        arrowInVisible = true;
        arrowOutVisible = true;
        setOnMousePressed(onMouseDraggedHandler);
        setOnMouseDragged(onMouseDraggedHandler);
        setOnMouseReleased(onMouseDraggedHandler);
    }

    TitledAnchorPane getContainerPane() {
        return containerPane;
    }

    LinkedList<ArrowLine> getArrowOut() {
        return arrowOut;
    }

    LinkedList<ArrowLine> getArrowIn() {
        return arrowIn;
    }

    public boolean isArrowOutVisible() {
        return arrowOutVisible;
    }

    public boolean isArrowInVisible() {
        return arrowInVisible;
    }

    void addArrowIn(ArrowLine line) {
        arrowIn.add(line);
    }

    void addArrowOut(ArrowLine line) {
        arrowOut.add(line);
    }

    void changeArrowOutVisibility() {
        arrowOutVisible = !arrowOutVisible;
        for (ArrowLine line : arrowOut)
            line.setVisible(arrowOutVisible);
    }

    int getObjectID() {
        return objectID;
    }

    /**
     * Given a node and heap state. Make a list of references from the node to some other objects.
     * @param node start node
     * @param currentInstruction current heap state
     * @return list of references
     */
    static List<Reference> getReferences(VNode node, Instruction currentInstruction) {
        ArrayList<Reference> refs = new ArrayList<>();
        if (node.isStack) {
            VStack stack = currentInstruction.getListOfVStack().get(node.getObjectID());

            for (int i = 0; i < stack.getVariables().size(); i++) {
                VVariable var = stack.getVariables().get(i);
                if (var.getValue().matches("\\(\\d+\\)")) {
                    int targetID = Integer.parseInt(var.getValue().substring(1, var.getValue().length() - 1));
                    refs.add(new Reference(targetID, getStackAttachableLocation(node, i)));
                }
            }
            return refs;
        } else {
            VObject object = currentInstruction.getMapOfVObject().get(node.getObjectID());
            if (object.getvObjType() == VObjectType.STRING) {
                return refs;
            } else if (object.getvObjType() == VObjectType.ARRAY) {
                List<String> elements = object.toList();
                for (int i = 0; i < elements.size(); i++) {
                    if (elements.get(i).matches("\\(\\d+\\)")) {
                        int targetId = Integer.parseInt(elements.get(i).substring(1, elements.get(i).length() - 1));
                        Point2D pos = getObjectAttachableLocation(node, i, 0, object.getvObjType());
                        refs.add(new Reference(targetId, pos));
                    }
                }
                return refs;
            } else if (object.getvObjType() == VObjectType.GENERIC) {
                int i = 0;
                for (Map.Entry<String, String> field : object.getFields().entrySet()) {
                    if (field.getValue().matches("\\(\\d+\\)")) {
                        int targetId = Integer.parseInt(field.getValue().substring(1, field.getValue().length() - 1));
                        Point2D pos = getStackAttachableLocation(node, i);
                        refs.add(new Reference(targetId, pos));
                    }
                    i++;
                }
                return refs;
            }
        }
        return refs;
    }

    static Point2D getStackAttachableLocation(VNode node, int varIndex) {
        if (varIndex < 0) {
            return node.localToParent(node.getContainerPane().getLayoutX(), node.getContainerPane().getLayoutY());
        } else {
            return node.localToParent(node.getContainerPane().getLayoutX() + node.getContainerPane().getWidth(),
                    node.getContainerPane().getLayoutY() + GridBox.DEFAULT_CELL_HEIGHT * varIndex + GridBox.DEFAULT_CELL_HEIGHT / 2 + TitledAnchorPane.TITLE_OVERHEAD);
        }
    }

    static Point2D getObjectAttachableLocation(VNode node, int col, int row, VObjectType type) {
        if (col < 0 || row < 0 || type == VObjectType.STRING) {
            return node.localToParent(node.getContainerPane().getLayoutX(), node.getContainerPane().getLayoutY());
        } else if (type == VObjectType.ARRAY) {
            GridBox box = (GridBox) node.getContainerPane().getContainerPane().getChildren().get(0);
            int hasLeftIndices = box.isLeftIndexColEnabled() ? 1 : 0;
            col += hasLeftIndices;
            return node.localToParent(node.getContainerPane().getLayoutX() + box.getMaxCellWidth() * col + box.getMaxCellWidth() / 2 + col,
                    node.getContainerPane().getLayoutY() + DEFAULT_CELL_HEIGHT * row + DEFAULT_CELL_HEIGHT / 2 + TitledAnchorPane.TITLE_OVERHEAD);
        } else if (type == VObjectType.GENERIC) {
            return VNode.getStackAttachableLocation(node, row);
        }
        return null;
    }

    /**
     * Mouse event handler on VNode
     */
    private EventHandler onMouseDraggedHandler = new EventHandler<MouseEvent>() {
        double mouseX;
        double mouseY;
        ContextMenu contextMenu;

        @Override
        public void handle(MouseEvent event) {
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED && event.getButton() == MouseButton.PRIMARY) {
                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
            } else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED && event.getButton() == MouseButton.PRIMARY) {
                double deltaX = event.getSceneX() - mouseX;
                double deltaY = event.getSceneY() - mouseY;

                VNode obj = (VNode) event.getSource();
                if (getLayoutX() + deltaX >= getParent().getLayoutX()
                        && getLayoutY() + deltaY >= getParent().getLayoutY()) {
                    obj.relocate(obj.getLayoutX() + deltaX, obj.getLayoutY() + deltaY);
                    for (ArrowLine out : arrowOut) {
                        Circle c = out.getStartPoint();
                        c.centerXProperty().set(c.centerXProperty().getValue() + deltaX);
                        c.centerYProperty().set(c.centerYProperty().getValue() + deltaY);
                    }
                    for (ArrowLine in : arrowIn) {
                        Circle c = in.getEndPoint();
                        c.centerXProperty().set(c.centerXProperty().getValue() + deltaX);
                        c.centerYProperty().set(c.centerYProperty().getValue() + deltaY);
                    }
                }

                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
                requestLayout();
                getParent().requestLayout();
            } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED && event.getButton() == MouseButton.SECONDARY) {
                //Show users options on right click
                if (contextMenu != null)
                    contextMenu.hide();

                contextMenu = new ContextMenu();
                String showHideTitle = containerPane.getTitleVisibility() ? "Hide" : "Show";
                MenuItem showHideTitleItem = new MenuItem(showHideTitle + " title");
                contextMenu.getItems().add(showHideTitleItem);
                showHideTitleItem.setOnAction(e -> VNode.this.getContainerPane().setTitleVisible(!VNode.this.getContainerPane().getTitleVisibility()));
                if (arrowOut.size() != 0) {
                    String showHideOut = arrowOutVisible ? "Hide" : "Show";
                    MenuItem arrowOutItem = new MenuItem(showHideOut + " arrows out");
                    contextMenu.getItems().add(arrowOutItem);
                    arrowOutItem.setOnAction(e -> VNode.this.changeArrowOutVisibility());
                }
                contextMenu.show(VNode.this, Side.RIGHT, 0, 0);
            }

        }
    };

    void showNode(boolean state) {
        setVisible(state);
        for (ArrowLine line : arrowOut)
            line.setVisible(state);
        if (!state) {
            for (ArrowLine line : arrowIn)
                line.setVisible(false);
            arrowInVisible = false;
        }
        arrowOutVisible = state;
    }

    void refreshSize() {
        autosize();
        containerPane.refreshSize();
    }
}