package visualizer;

import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import parser.VVariable;

/**
 * Cell Factory for Stack list.
 * <p>
 * Created by Lam Cao on 11/27/2017.
 */
public class VariableListCellFactory implements Callback<ListView<VVariable>, ListCell<VVariable>> {

    private VisualizerUI visualizer;

    VariableListCellFactory(VisualizerUI visualizer) {
        this.visualizer = visualizer;
    }

    @Override
    public ListCell<VVariable> call(ListView<VVariable> view) {
        return new VariableListCell();
    }

    class VariableListCell extends ListCell<VVariable> {
        @Override
        public void updateItem(VVariable item, boolean empty) {
            super.updateItem(item, empty);

            if (!empty && item != null)
                this.setText(item.toString());
            else
                this.setText("");
            setGraphic(null);
            setOnMouseClicked((MouseEvent event) -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    if (item == null)
                        return;
                    if (!item.getValue().matches("\\(\\d+\\)"))
                        return;
                    int objID = Integer.parseInt(item.getValue().substring(1, item.getValue().length() - 1));
                    //Show users options on right click
                    ContextMenu contextMenu = new ContextMenu();
                    MenuItem menuItem = new MenuItem("Show/Hide...");
                    contextMenu.getItems().add(menuItem);
                    menuItem.setOnAction(e -> visualizer.getCanvasPane().setObjectVisibility(objID));
                    contextMenu.show(this, Side.RIGHT, 0, 0);
                }
            });
        }
    }
}
