package visualizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SplitPane;
import parser.Instruction;
import parser.VStack;
import parser.VVariable;
import utils.TitledListView;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;


/**
 * Canvas for stack frames and objects
 * All instructions will be passed here to process and draw accordingly
 * <p>
 * Created by Lam Cao
 */
public class VisualizerUI extends SplitPane implements Observer {

    @FXML
    private TitledListView<VStack> stackListView;
    @FXML
    private TitledListView<VVariable> variableListView;
    @FXML
    private CanvasPane canvasPane;

    private ObservableList<VVariable> selectedStackVars;
    private VisualizerModel model;

    public VisualizerUI() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/visualizer/VisualizerUI.fxml")
        );

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        canvasPane.initializePane();
        //canvasPane.setOnMouseDragged(event -> requestRefreshArrows());
        model = new VisualizerModel();
        model.addObserver(canvasPane);
        selectedStackVars = FXCollections.observableArrayList();

        //set up stack and variable list views
        stackListView.setTitleLabel("Stack Frames");
        stackListView.getItemListView().setCellFactory(new StackListCellFactory(this));
        stackListView.getItemListView().setItems(model.getStacks());
        stackListView.getItemListView().getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> onStackSelected(newValue));

        variableListView.setTitleLabel("Variables");
        variableListView.getItemListView().setCellFactory(new VariableListCellFactory(this));
        variableListView.getItemListView().setItems(selectedStackVars);
    }

    /**
     * All instruction will be passed to here to extract and process information
     *
     * @param o   Observable object
     * @param arg new instruction to process
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg != null && arg instanceof Instruction) {
            model.setInstructionData((Instruction) arg);
            stackListView.getItemListView().getSelectionModel().selectLast();
            model.notifyObservers();
        }
    }

    /**
     * clear out all items on canvas
     */
    public void reset() {
        model.resetData();
        canvasPane.clearAllChildren();
    }

    private void onStackSelected(VStack stack) {
        selectedStackVars.clear();
        if (stack != null)
            selectedStackVars.addAll(stack.getVariables());
    }

    CanvasPane getCanvasPane() {
        return canvasPane;
    }
}