package visualizer;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;
import parser.*;
import utils.ArrowLine;

import java.util.*;

/**
 * Canvas on which objects and stacks are drawn.
 * <p>
 * Created by Lam cao
 */
public class CanvasPane extends AnchorPane implements Observer {

    private Instruction heapState;
    private HashMap<Integer, VNode> visualizedStacks = new HashMap<>();
    //note that visualizedObjects map object IDs to Views instead of indices
    private HashMap<Integer, VNode> visualizedObjects = new HashMap<>();
    private Group stackPane;
    private Group objectPane;

    void initializePane() {
        stackPane = new Group();
        objectPane = new Group();
        objectPane.setPickOnBounds(false);
        stackPane.setPickOnBounds(false);
        getChildren().add(stackPane);
        getChildren().add(objectPane);
    }

    private void updateArrows() {
        clearAllArrows();
        for (int i = 0; i < visualizedStacks.size(); i++) {
            VNode node = visualizedStacks.get(i);
//            if (!node.isVisible() || !node.isArrowOutVisible())
//                continue;
            List<Reference> refs = VNode.getReferences(node, heapState);
            for (Reference ref : refs) {
                int targetID = ref.getTargetObjectID();
                Point2D startPoint = ref.getAttachableLocation();
                VNode targetObj = visualizedObjects.get(targetID);
                VObject vObj = heapState.getMapOfVObject().get(targetObj.getObjectID());
                Point2D endPoint = VNode.getObjectAttachableLocation(targetObj, -1, -1, vObj.getvObjType());
                ArrowLine line = new ArrowLine(startPoint, endPoint);
                getChildren().add(line);
                node.addArrowOut(line);
                targetObj.addArrowIn(line);
                if (!targetObj.isVisible() || !targetObj.isArrowInVisible() || !node.isArrowOutVisible())
                    line.setVisible(false);
            }
        }

        for (VNode node : visualizedObjects.values()) {
//            if (!node.isVisible() || !node.isArrowOutVisible())
//                continue;
            List<Reference> refs = VNode.getReferences(node, heapState);
            for (Reference ref : refs) {
                int targetID = ref.getTargetObjectID();
                Point2D startPoint = ref.getAttachableLocation();
                VNode targetObj = visualizedObjects.get(targetID);
                VObject vObj = heapState.getMapOfVObject().get(targetObj.getObjectID());
                Point2D endPoint = VNode.getObjectAttachableLocation(targetObj, -1, -1, vObj.getvObjType());
                ArrowLine line = new ArrowLine(startPoint, endPoint);
                getChildren().add(line);
                node.addArrowOut(line);
                targetObj.addArrowIn(line);
                if (!targetObj.isVisible() || !targetObj.isArrowInVisible() || !node.isArrowOutVisible())
                    line.setVisible(false);
            }
        }
    }

    private void updateAllStacks() {
        List<VStack> stacks = heapState.getListOfVStack();
        StackBuilder stackBuilder = NodeBuilder.getStackBuilder();
        for (int i = 0; i < stacks.size(); i++) {
            if (visualizedStacks.containsKey(i)) {
                VNode node = visualizedStacks.get(i);
                stackBuilder.updateNode(node, i, heapState);
                node.refreshSize();
            } else {
                VNode newNode = stackBuilder.makeNode(i, heapState);
                if (visualizedStacks.size() != 0) {
                    VNode lastNode = (VNode) stackPane.getChildren().get(visualizedStacks.size() - 1);
                    Point2D pos = VNode.getStackAttachableLocation(lastNode, -1);
                    newNode.setLayoutX(pos.getX());
                    newNode.setLayoutY(pos.getY() + lastNode.getContainerPane().getHeight());
                }
                stackPane.getChildren().add(newNode);
                visualizedStacks.put(i, newNode);
                newNode.refreshSize();
            }
        }

        if (visualizedStacks.size() > stacks.size()) {
            for (int i = visualizedStacks.size() - 1; i > stacks.size() - 1; i--) {
                stackPane.getChildren().remove(i);
                visualizedStacks.remove(i);
            }
        }
    }

    private List<VNode> updateAllObjects() {
        ObjectBuilder objBuilder = NodeBuilder.getObjectBuilder();
        List<VObject> vObjects = heapState.getListOfVObject();
        List<VNode> newObjectList = new LinkedList<>();
        for (VObject vObj : vObjects) {
            if (visualizedObjects.containsKey(vObj.getID())) {
                VNode oldNode = visualizedObjects.get(vObj.getID());
                objBuilder.updateNode(oldNode, vObj.getID(), heapState);
                oldNode.refreshSize();
            } else {
                VNode node = objBuilder.makeNode(vObj.getID(), heapState);
                objectPane.getChildren().add(node);
                visualizedObjects.put(vObj.getID(), node);
                newObjectList.add(node);
                node.refreshSize();
            }
        }

        if (visualizedObjects.size() > vObjects.size()) {
            Map<Integer, VObject> map = heapState.getMapOfVObject();
            HashSet<Integer> idToRemove = new HashSet<>();
            for (int id : visualizedObjects.keySet()) {
                if (!map.containsKey(id))
                    idToRemove.add(id);
            }

            for (int id : idToRemove) {
                visualizedObjects.remove(id);
            }
            objectPane.getChildren().clear();
            objectPane.getChildren().addAll(visualizedObjects.values());
        }

        return newObjectList;
    }

    void clearAllChildren() {
        visualizedStacks.clear();
        visualizedObjects.clear();
        stackPane.getChildren().clear();
        objectPane.getChildren().clear();
        clearAllArrows();
    }

    private void clearAllArrows() {
        ArrayList<Integer> nums = new ArrayList<>();
        for (int i = getChildren().size() - 1; i >= 0; i--) {
            if (getChildren().get(i) instanceof ArrowLine)
                nums.add(i);
        }

        for (int n : nums) {
            getChildren().remove(n);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof VisualizerModel) {
            VisualizerModel model = (VisualizerModel) o;
            heapState = model.getCurrentInstruction();
            updateAllStacks();
            List<VNode> newObjects = updateAllObjects();
            updateArrows();
            autoArrangeNewObjects(newObjects);
            if(newObjects.size() > 0)
                updateArrows();
        }
    }

    private void autoArrangeNewObjects(List<VNode> newObjects) {
        for (VNode node : newObjects) {
            if (node.getArrowIn().size() > 0) {
                ArrowLine line = node.getArrowIn().get(0);
                double startX = line.getStartPoint().getCenterX();
                double startY = line.getStartPoint().getCenterY();
                node.relocate(startX + 30f, startY + 20f);
                Point2D point = VNode.getStackAttachableLocation(node, -1);
                line.getEndPoint().centerXProperty().set(point.getX());
                line.getEndPoint().centerYProperty().set(point.getY());
            }
        }
    }

    void setStackVisibility(int index) {
        changeNodeVisibility(true, index);
    }

    void setObjectVisibility(int objID) {
        changeNodeVisibility(false, objID);
    }

    private void changeNodeVisibility(boolean isStack, int id) {
        VNode node = isStack ? visualizedStacks.get(id) : visualizedObjects.get(id);
        node.showNode(!node.isVisible());
        //updateArrows(heapState);
    }
}