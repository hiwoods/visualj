package visualizer;

import parser.Instruction;

/**
 * Generate a representation for an object given its ID and current heap state
 *
 * Created by Lam Cao
 */
public interface NodeBuilder {
    static StackBuilder getStackBuilder() {
        return new StackBuilder();
    }

    static ObjectBuilder getObjectBuilder() {
        return new ObjectBuilder();
    }

    /**
     * Generate new representation for an object
     * @param id id of the object
     * @param instruction current heap state
     * @return appropriate representation of the object
     */
    VNode makeNode(int id, Instruction instruction);

    /**
     * Mostly
     * @param oldNode
     * @param id
     * @param instruction
     * @return
     */
    VNode updateNode(VNode oldNode, int id, Instruction instruction);
} 