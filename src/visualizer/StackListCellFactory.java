package visualizer;

import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import parser.VStack;


/**
 * Cell Factory for Stack list.
 * <p>
 * Created by Lam Cao on 11/27/2017.
 */
public class StackListCellFactory implements Callback<ListView<VStack>, ListCell<VStack>> {

    private VisualizerUI visualizer;

    StackListCellFactory(VisualizerUI visualizerUI) {
        visualizer = visualizerUI;
    }

    @Override
    public ListCell<VStack> call(ListView<VStack> view) {
        return new StackListCell();
    }

    class StackListCell extends ListCell<VStack> {
        @Override
        public void updateItem(VStack item, boolean empty) {
            super.updateItem(item, empty);

            if (!empty && item != null)
                this.setText(item.getStackName());
            else
                this.setText("");
            setGraphic(null);
            setOnMouseClicked((MouseEvent event) -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    if (item == null)
                        return;
                    //Show users options on right click
                    ContextMenu contextMenu = new ContextMenu();
                    MenuItem menuItem = new MenuItem("Show/Hide...");
                    contextMenu.getItems().add(menuItem);
                    menuItem.setOnAction(e -> visualizer.getCanvasPane().setStackVisibility(getIndex()));
                    contextMenu.show(this, Side.RIGHT, 0, 0);
                }
            });
        }
    }

}
