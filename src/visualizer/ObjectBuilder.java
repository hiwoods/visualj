package visualizer;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import parser.*;
import utils.GridBox;
import utils.TitledAnchorPane;

import java.util.ArrayList;
import java.util.List;

import static utils.GridBox.DEFAULT_CELL_HEIGHT;
import static utils.GridBox.DEFAULT_CELL_HIGHLIGHT_COLOR;

/**
 * Create new and update object nodes to be drawn on canvas.
 * <p>
 * Created by Lam Cao
 *
 * @see visualizer.NodeBuilder
 */
public class ObjectBuilder implements NodeBuilder {

    @Override
    public VNode makeNode(int id, Instruction instruction) {
        VObject object = instruction.getMapOfVObject().get(id);
        List<String> elements;
        if (object.getvObjType() == VObjectType.STRING) {
            elements = new ArrayList<>(1);
            elements.add(object.getRawData());
        } else
            elements = object.toList();
        boolean verticalLayout = object.getvObjType() == VObjectType.GENERIC;
        double minWidth = computeNeededWidth(elements);
        TitledAnchorPane nodePane = new TitledAnchorPane();
        nodePane.setTitleLabel(object.getTypeName() + "@" + object.getID());
        GridBox box = new GridBox(minWidth, DEFAULT_CELL_HEIGHT, 1, 1);

        ArrayList<StackPane> paneList = new ArrayList<>();
        for (String str : elements) {
            StackPane pane = new StackPane();
            Rectangle rect = box.makeRectangle(box.getMaxCellWidth(), DEFAULT_CELL_HEIGHT, DEFAULT_CELL_HIGHLIGHT_COLOR, true);
            pane.getChildren().add(rect);
            pane.getChildren().add(new Label(str));
            paneList.add(pane);
        }

        if(elements.size() == 0)
            nodePane.showBorder(true);
        else
            nodePane.showBorder(false);
        if (verticalLayout) {
            for (StackPane pane : paneList)
                box.addRows(pane);
        } else {
            box.addRows(paneList);
            if (object.getvObjType() == VObjectType.ARRAY)
                box.setBottomIndexRowVisible(true);
        }
        box.refreshSize();
        nodePane.refreshSize();
        nodePane.getContainerPane().getChildren().add(box);
        return new VNode(nodePane, id, false);
    }

    @Override
    public VNode updateNode(VNode oldNode, int id, Instruction instruction) {
        VObject object = instruction.getMapOfVObject().get(id);
        TitledAnchorPane titledPane = oldNode.getContainerPane();
        GridBox content = (GridBox) titledPane.getContainerPane().getChildren().get(0);
        List<String> elements;

        if (object.getvObjType() == VObjectType.STRING) {
            elements = new ArrayList<>(1);
            elements.add(object.getRawData());
        } else
            elements = object.toList();

        double maxWidth = computeNeededWidth(elements);
        content.setMaxCellWidth(maxWidth);

        int col = content.isLeftIndexColEnabled() ? 1 : 0;
        int i;
        for (i = col; i < elements.size(); i++) {
            try {
                StackPane cell;
                if (object.getvObjType() == VObjectType.GENERIC) {
                    HBox row = (HBox) content.getChildren().get(i);
                    cell = (StackPane) row.getChildren().get(col);
                } else {
                    HBox row = (HBox) content.getChildren().get(0);
                    cell = (StackPane) row.getChildren().get(i);
                }
                Label cellCont = (Label) cell.getChildren().get(1);
                Rectangle rect = (Rectangle) cell.getChildren().get(0);
                if (!cellCont.getText().equals(elements.get(i))) {
                    rect.setFill(DEFAULT_CELL_HIGHLIGHT_COLOR);
                    cellCont.setText(elements.get(i));

                } else {
                    rect.setFill(Color.WHITE);
                }
            } catch (IndexOutOfBoundsException e) {
                StackPane newCell = new StackPane();
                Rectangle rect = content.makeRectangle(content.getMaxCellWidth(), DEFAULT_CELL_HEIGHT, DEFAULT_CELL_HIGHLIGHT_COLOR, true);
                Label label = new Label(elements.get(i));
                newCell.getChildren().addAll(rect, label);
                if (object.getvObjType() == VObjectType.GENERIC) {
                    content.addRows(newCell);
                } else {
                    HBox row = (HBox) content.getChildren().get(0);
                    row.getChildren().add(newCell);
                }
            }
        }
        if (object.getvObjType() == VObjectType.GENERIC) {
            if (content.getNumRow() > elements.size()) {
                for (; i < content.getNumRow(); i++) {
                    content.removeRow(content.getNumRow() - 1);
                }
            }
        } else {
            HBox row = (HBox) content.getChildren().get(0);
            if (row.getChildren().size() - col > elements.size()) {
                for (; i < row.getChildren().size(); i++) {
                    row.getChildren().remove(row.getChildren().size() - 1);
                }
            }
        }
        content.refreshSize();
        return oldNode;
    }

    private double computeNeededWidth(List<String> list) {
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        double maxWidth = 0;
        for (String str : list) {
            double width = Math.ceil(fontLoader.computeStringWidth(str, Font.getDefault()));
            if (width > maxWidth)
                maxWidth = width;
        }
        return maxWidth;
    }
}