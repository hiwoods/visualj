package visualizer;

import javafx.geometry.Point2D;

/**
 * A wrapper for references
 * <p>
 * Created by Lam Cao
 */
public class Reference {

    private int targetObjectID;
    private Point2D attachableLocation;

    Reference(int targetObjectID, Point2D attachableLocation) {
        this.targetObjectID = targetObjectID;
        this.attachableLocation = attachableLocation;
    }

    public int getTargetObjectID() {
        return targetObjectID;
    }

    public Point2D getAttachableLocation() {
        return attachableLocation;
    }
}